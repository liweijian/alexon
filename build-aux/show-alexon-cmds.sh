#  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
#  Copyright (C) 2023
#      SymeCloud Limited
#  This is free software: you can redistribute it and/or modify
#  it under the terms of the GNU General Public License published by
#  the Free Software Foundation, either version 3 of the License, or
#  (at your option) any later version.

#  This is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.

#  You should have received a copy of the GNU General Public License
#  along with this program.
#  If not, see <http://www.gnu.org/licenses/>.

# install in /etc/bash_completion.d/ or your personal directory

_alexon()
{
    local cur=`_get_cword`
    local cmds=`alexon list-all-cmds`
    COMPREPLY=()

    if [ "$COMP_CWORD" == 1 ]; then
        COMPREPLY=($(compgen -W "$cmds" -- "$cur"))
        return
    elif [ "$COMP_CWORD" == 2 ]; then
        # TODO: support cmd options complete
        local second=${COMP_WORDS[COMP_CWORD-1]}
        # create draw help migrate version work
        local res=`alexon $second --cmd-list`
        COMPREPLY=($(compgen -W "$res" -- "$cur"));
        return 0;
    elif [ "$COMP_CWORD" == 3 ]; then
        local second=${COMP_WORDS[1]}
        local third=${COMP_WORDS[2]}
        if [ "$second" == "app" ] && [ "$third" == "create" ]; then
            local res=`ls | grep -E "*.(json|yaml)$"`
            COMPREPLY=($(compgen -W "$res" -- "$cur"));
            return 0;
        elif [ "$second" == "cluster" ] && [ "$third" == "worker" ]; then
            local res=`ls | grep -E "*.(json|yaml)$"`
            COMPREPLY=($(compgen -W "$res" -- "$cur"));
            return 0;
        else
            local res=`alexon $second $third --list-options`
            COMPREPLY=($(compgen -W "$res" -- "$cur"));
            return 0;
        fi
    fi
    return
}

complete $filenames -F _alexon alexon

# vim:ts=2 sw=2 et syn=sh
