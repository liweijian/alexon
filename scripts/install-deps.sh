#!/bin/sh

force="no"

# Install yq
install_yq() {
    if [ ! -f /usr/bin/yq ]; then
        echo "Installing yq"
        if [ ! -f ./yq_linux_amd64 ]; then
            wget -c https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64
        fi
        mv yq_linux_amd64 /usr/bin/yq
        chmod +x /usr/bin/yq
    fi
}

# Install helm
install_helm() {
    if [ ! -f /usr/bin/helm ] || [ "$force" == "yes" ]; then
        echo "Installing helm"
        wget https://get.helm.sh/helm-v3.11.3-linux-amd64.tar.gz -O - | tar -xzO linux-amd64/helm > /usr/bin/helm &&\
            chmod +x /usr/bin/helm
    fi
}

# Install Docker for k3s
# NOTE: We don't install it officially on host machine, only the binary is requried.
#       The k3s will be actually run in container node.
install_docker() {
    if [ ! -f /usr/bin/docker ] || [ "$force" == "yes" ]; then
        echo "Installing docker"
        curl https://get.docker.com -o - | sh -s - get-docker.sh --mirror Aliyun
    fi
}

# Install k3s with Docker (rather than containerd)
install_k3s() {
    if [ ! -f /usr/local/bin/k3s ] || [ "$force" == "yes" ]; then
        echo "Installing k3s"
        # download till finished
        # if k3s doesn't exists, then download it.
        if [ ! -f ./k3s ]; then
            while true;
            do wget -c https://github.com/k3s-io/k3s/releases/download/v1.25.11%2Bk3s1/k3s
               if [ $? -eq 0 ];
               then break;
               else sleep 1;
               fi;
            done
        fi
        mv k3s /usr/local/bin
        chmod +x /usr/local/bin/k3s
    fi
}

install_guile_json() {
    echo "Installing guile-json"
    if [ ! -f ./4.7.3.tar.gz ]; then
        wget -c https://github.com/aconchillo/guile-json/archive/refs/tags/4.7.3.tar.gz
    fi
    tar -xvf 4.7.3.tar.gz &&\
        cd guile-json-4.7.3 &&\
        autoreconf -vif &&\
        ./configure --prefix=/usr &&\
        make &&\
        make install &&\
        cd .. &&\
        rm -fr guile-json-4.7.3 4.7.3.tar.gz
}

install_deps() {
    install_guile_json &&\
        install_yq &&\
        install_k3s

    if [ $? -ne 0 ]; then
        echo "Failed to install dependencies"
        exit 1
    fi

    # install_helm
    if [ "$no_docker" != "yes" ]; then
        install_docker
    fi

}

check_uid() {
    if [ "$(id -u)" != "0" ]; then
        echo "This script must be run as root" 1>&2
        exit 0
    fi
}


check_uid

for arg in "$@"
do
    if [ "$arg" == "--no-docker" ]; then
        no_docker="yes"
    fi

    if [ "$arg" = "--upgrade" ] || [ "$1" == "-u" ]; then
        force="yes"
    fi
done

install_deps
