;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon commands)
  #:use-module (alexon version)
  #:use-module (srfi srfi-1)
  #:export (find-command
            no-command?
            no-command-args?
            announce-head
            announce-foot
            option-spec->str))

(define (no-command? args)
  (< (length args) 2))

(define (no-command-args? args)
  (< (length args) 3))

(define command-path '(alexon commands))

(define (find-command name)
  (and=> (resolve-module `(,@command-path ,(string->symbol name)) #:ensure #f)
         (lambda (m) (values name m))))

(define announce-head
  "
Alexon is a Cloud Native Operating System.
SymeCloud Limited
")

(define announce-foot
  (format #f "~%~a~%Version: ~a.~%God bless hacking.~%~%" "GPLv3+" alexon-version))

(define (option-spec->str spec)
  (fold (lambda (value acc) (string-append acc " --" (symbol->string (car value))))
        ""
        spec))
