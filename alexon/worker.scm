;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon worker)
  #:use-module (alexon utils)
  #:use-module (alexon verify)
  #:use-module (alexon manifest)
  #:use-module (json)
  #:use-module ((rnrs) #:select (define-record-type))
  #:export (worker-constructor
            read-worker-manifest
            worker-verifier))

;; The worker in Alexon is a simplified node definition, we don't provide full configures
;; to control the node completely, instead, we provide a minimum set to simplify the
;; deployment.
;; Users may use `node modify' command to update the node configuration more precisely.
;; For example, setup the resources, cpu, memory, etc.


;; TODO: check each field in the worker definition.
(define (ports-checker)
  (lambda (ports)
    (if (not (vector? ports))
        (error "ports-checker: ports must be a vector!" ports)
        (for-each (lambda (p)
                    (when (not (list? p))
                      (error "port must be a number")))
                  (vector->list ports)))))

(define *default-worker-values*
  `(("kind" ,(must-specified "Kind") "Worker")
    ("type" ,(string-checker "type") "worker")
    ("cluster" ,(must-specified "cluster") #f)
    ("name" ,(must-specified "name") #f)
    ("image" ,(must-specified "image") #f)
    ("labels" ,(list-checker "labels") ())
    ("workdir" ,(string-checker "workdir") "/")
    ("env" ,(list-checker "env") ())
    ("deps" ,(vector-checker "deps") #())
    ("cmd" ,(string->list/checker "cmd") "")
    ("ports" ,(ports-checker) #())
    ("mounts" ,(vector-checker "mounts") #())
    ("replicas" ,(number-checker "replicas") 1)
    ("entrypoint" ,(string-checker "entrypoint") "")
    ("pipeline" ,(list-checker "pipeline") ())
    ))

(define worker-verifier (make-general-verifier 'worker *default-worker-values*))

(define (worker-constructor node-def)
  (fill-config-with-defaults 'worker-constructor *default-worker-values* node-def))

;; NOTE: each worker manifest contains only one node definition.
(define (read-worker-manifest path)
  (let* ((manifest (read-manifest-file path))
         (cluster (or (current-cluster)
                      (sxml-query "cluster" manifest)))
         (all `(("kind" . "Worker")
                ("cluster" . ,cluster)
                ("type" . "worker")
                ,@(sxml-query "nodeDef" manifest))))
    (verify-manifest worker-verifier all)
    (worker-constructor all)))
