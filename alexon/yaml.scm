;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon yaml)
  #:use-module (alexon utils)
  #:use-module (alexon cli)
  #:use-module (json)
  #:use-module ((rnrs) #:select (get-string-all))
  #:export (yaml-file->scm
            single-yaml-string->scm
            scm->yaml-file
            scm->yaml-string
            combine-and-convert-to-yaml-string))

(define (yaml-split yaml)
  (let ((len (string-length yaml)))
    (let lp ((pos 0) (lpos 0) (ret '()))
      (cond
       ((= pos len)
        (let ((final (substring/shared yaml lpos pos)))
          (if (string=? "" final)
              (values 1 yaml)
              (values (1+ (length ret))
                      (reverse (cons final ret))))))
       ((char=? #\- (string-ref yaml pos))
        (let ((n pos))
          (cond
           ((>= (+ n 3) len)
            (error "yaml-split: unexpected end of yaml"
                   (substring/shared yaml n (+ n 3))))
           ((string=? "---" (substring/shared yaml n (+ n 3)))
            (lp (+ n 3) (+ n 3) ; skip ---
                (cons (string-trim-both (substring/shared yaml lpos pos))
                      ret)))
           (else (lp (1+ pos) lpos ret)))))
       (else (lp (1+ pos) lpos ret))))))

(define (single-yaml-string->scm yaml)
  (let* ((tmp-port (mktemp))
         (file (port-filename tmp-port)))
    (display yaml tmp-port)
    (close tmp-port)
    (let ((ret (cli-run*
                yq -o json -p yaml -P "." ,file)))
      (cond
       ((zero? (<cli>-status ret))
        (json-string->scm (<cli>-result ret)))
       (else
        (error
         (format #f "[errno: ~a] yaml-string->scm: ~a"
                 (<cli>-status ret) (<cli>-result ret))))))))

(define (yaml-file->scm file)
  (call-with-values
      (lambda ()
        (yaml-split (call-with-input-file file get-string-all)))
    (lambda (cnt str)
      (if (= cnt 1)
          (single-yaml-string->scm (car str))
          (map single-yaml-string->scm str)))))

(define (scm->yaml-string scm)
  (let* ((json-string (scm->json-string scm))
         (tmp-port (mktemp))
         (file (port-filename tmp-port)))
    (display json-string tmp-port)
    (close tmp-port)
    (let ((ret (cli-run*
                yq -o yaml -p json -P "." ,file)))
      (cond
       ((zero? (<cli>-status ret))
        (<cli>-result ret))
       (else
        (error
         (format #f "[errno: ~a] scm->yaml-string: ~a"
                 (<cli>-status ret) (<cli>-result ret))))))))

(define (scm->yaml-file scm filename)
  (let ((yaml-string (scm->yaml-string scm)))
    (with-output-to-file filename (lambda () (display yaml-string)))))

(define (combine-and-convert-to-yaml-string lst)
  (string-join (map scm->yaml-string lst) "\n---\n"))
