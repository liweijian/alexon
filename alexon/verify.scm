;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon verify)
  #:use-module (alexon utils)
  #:use-module (alexon container)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 format)
  #:export (verify-app
            verify-worker
            verify-api-version
            verify-kind
            verify-manifest
            verify-port
            verify-cluster))

(define *prefix* "syme.dev/alexon")
(define (gen type version) (format #f "~a/~a/~a" *prefix* type version))

(define *app-version* (gen 'app 'v1-alpha))
(define *worker-version* (gen 'worker 'v1-alpha))

(define (verify-api-version ver)
  (cond
   ((and (string? ver) (string-match "^syme.dev/alexon/([^/]+)/v(.+)$" ver)) #t)
   (else (error (format #f "apiVersion `~a' is not valid!" ver)))))

(define *kind-list* '("Application" "Worker"))
(define (verify-kind kind)
  (cond
   ((and (string? kind) (member kind *kind-list*)) #t)
   (else (error (format #f "kind `~a' should be `~{~a~^, ~}`." kind *kind-list*)))))

;; NOTE:
;; 1. Alexon doesn't check apiVersion, since users should self-check.
;; 2. If the fields checking failed, users should check the spec.
(define (make-verifier type)
  (lambda (verifier manifest)
    ;; TODO: add type specific check
    (verifier manifest)))

(define (verify-app verifier manifest)
  ((make-verifier 'app) verifier manifest))

(define (verify-worker verifier manifest)
  ((make-verifier 'worker) verifier manifest))

(define (verify-manifest verifier manifest)
  (let ((kind (sxml-query "kind" manifest)))
    (cond
     ((string=? kind "Application") (verify-app verifier manifest))
     ((string=? kind "Worker") (verify-worker verifier manifest))
     (else (error (format #f "unknown kind: `~a'" kind))))))

(define (verify-port port)
  (when (is-socket-port-occupied? (string->number (current-publish-port)))
    (error (format #f "Port ~a was occupied, please try another!"
                   (current-publish-port)))))

(define (verify-cluster name)
  (let ((cluster-name (format #f "alexon-~a-server-0" name)))
    (when (container-node-inspect cluster-name #:quiet? #t)
      (format #t "Cluster name `~a' was occupied!~%" cluster-name)
      (format #t "Please try another name, or remove the cluster with `alexon cluster remove ~a -f'~%" name)
      (exit -11))))
