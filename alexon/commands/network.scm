;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon commands network)
  #:use-module (alexon utils)
  #:use-module (alexon commands)
  #:use-module (alexon container)
  #:use-module (ice-9 getopt-long)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (json))

(define list-options-spec
  `((json (value #f))))
(define (list-networks . args)
  (define (ip-type network)
    ;; Check EnableIPv6, if yes then "IPv6", otherwise "IPv4"
    (if (sxml-query "EnableIPv6" network)
        "IPv6"
        "IPv4"))
  (let* ((options (if (null? args) '() (getopt-long args list-options-spec)))
         (args (option-ref options '() '()))
         (networks (container-network-list)))
    (cond
     ((zero? (vector-length networks))
      (display "No network found.\n"))
     ((just-output-json?)
      (print-json-info networks))
     (else
      (format #t "ID~/NAME~3/DRIVER~/  SCOPE~2/TYPE~2/START~%")
      (for-each
       (lambda (i)
         (when (equal? (sxml-query "Labels/app" (vector-ref networks i)) "alexon")
           (let* ((network (vector-ref networks i))
                  (id (substring/shared (assoc-ref network "Id") 0 6))
                  (name (sxml-query "Name" network))
                  (driver (sxml-query "Driver" network))
                  (scope (sxml-query "Scope" network))
                  (ip (ip-type network))
                  (start (sxml-query "Created" network)))
             (format #t "~6a~/~20a~/~6a    ~8a~/~10a~/~36a~%"
                     id name driver scope ip start))))
       (iota (vector-length networks)))))))

(define create-options-spec
  `((driver (value #t))
    (subnet (value #f))
    (gateway (value #f))
    (ip-range (value #f))))
(define (create-network . args)
  (let* ((options (if (null? args) '() (getopt-long args create-options-spec)))
         (args (option-ref options '() '())))
    (parameterize ((current-network-driver (option-ref options 'driver "bridge"))
                   (current-network-subnet (option-ref options 'subnet #f))
                   (current-network-gateway (option-ref options 'gateway #f))
                   (current-network-ip-range (option-ref options 'ip-range #f)))
      (container-network-create (car args)))))

(define remove-options-spec
  `((force (single-char #\f) (value #f))))
(define (remove-network . args)
  (let* ((options (if (null? args) '() (getopt-long args remove-options-spec)))
         (args (option-ref options '() '())))
    (container-network-remove (car args))))

(define *help-content*
  "
Usage: alexon network subcommand [options]

Subcommands:
  list
  create
  remove
")

(define (show-help . _)
  (display
   (string-append announce-head
                  *help-content*
                  announce-foot)))

(define option-spec
  `((json (value #f))))

(define (show-all-cmds)
  (format #t "~{~a~^ ~}" '(list create remove)))

(define (handle-network . args)
  "Manipulate nodes operations."
  ;; handle get-options
  (match args
    (()
     (show-help))
    (("--cmd-list")
     (show-all-cmds))
    (("list" "--list-options")
     (display (option-spec->str list-options-spec)))
    (("list" . _)
     (apply list-networks args))
    (("create" "--list-options")
     (display (option-spec->str create-options-spec)))
    (("create" . _)
     (apply create-network args))
    (("remove" "--list-options")
     (display (option-spec->str remove-options-spec)))
    (("remove" . _)
     (apply remove-network args))
    (else
     (show-help))))

(define %summary "Manipulate container networks.")
(define main handle-network)
