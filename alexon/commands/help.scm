;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon commands help)
  #:use-module (alexon utils)
  #:use-module (alexon commands)
  #:use-module (alexon version)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 ftw)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 format))

(define (get-all-commands)
  (map remove-ext
       (scandir (format #f "~a/alexon/commands" alexon-modpath)
                (lambda (f)
                  (not (or (string=? f ".")
                           (string=? f "..")
                           (string-match ".~$" f)))))))

(define (get-info cmd)
  (let ((sym (string->symbol cmd)))
    (module-ref (resolve-module `(alexon commands ,sym)) '%summary)))

(define (show-cmds-info)
  (let ((cmds (get-all-commands)))
    (apply string-append
           (map (lambda (cmd)
                  (format #f "  ~a~20t~a~%"
                          cmd (get-info cmd)))
                cmds))))

(define (gen-help-str)
  (string-append announce-head
                 "\nCommands:\n"
                 (show-cmds-info)
                 announce-foot))

(define (show-help . _) (display (gen-help-str)))

(define %summary "Show this screen.")
(define main show-help)
