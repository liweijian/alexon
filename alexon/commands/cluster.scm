;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon commands cluster)
  #:use-module (alexon utils)
  #:use-module (alexon commands)
  #:use-module (alexon container)
  #:use-module (alexon worker)
  #:use-module (alexon verify)
  #:use-module (alexon dns)
  #:use-module (ice-9 getopt-long)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (json))

(define list-options-spec
  `((json (value #f))))
(define (list-clusters . args)
  (let* ((options (if (null? args) '() (getopt-long args list-options-spec)))
         (args (option-ref options '() '()))
         (clusters (container-cluster-list)))
    (parameterize ((just-output-json? (option-ref options 'json #f)))
      (cond
       ((null? clusters)
        (display "No clusters found.\n"))
       ((just-output-json?)
        (print-json-info clusters #:is-list? #t))
       (else
        (format #t "NAME~/SERVERS~3/AGENTS~2/LOADBALANCER~%")
        (for-each
         (lambda (cluster)
           (when (equal? (sxml-query "Labels/app" cluster) "alexon")
             (format #t "~20a~6a~6a~10a"
                     (sxml-query "Name" cluster)
                     (sxml-query "Servers" cluster)
                     (sxml-query "Agents" cluster)
                     (sxml-query "LoadBalancer" cluster))))
         clusters))))))

(define create-options-spec
  `((driver (value #t))
    (subnet (value #t))
    (gateway (value #t))
    (ip-range (value #t))
    (servers (value #t))
    (publish-port (single-char #\p) (value #t))
    (volume-size (value #t))))
(define (create-cluster . args)
  (let* ((options (if (null? args) '() (getopt-long args create-options-spec)))
         (args (option-ref options '() '())))
    (parameterize ((current-network-driver (option-ref options 'driver "bridge"))
                   (current-network-subnet (option-ref options 'subnet #f))
                   (current-network-gateway (option-ref options 'gateway #f))
                   (current-network-ip-range (option-ref options 'ip-range #f))
                   (current-volume-driver (option-ref options 'driver "local"))
                   ;; the default volume driver is "local" which doesn't support quota size.
                   (current-volume-size (option-ref options 'volume-size #f))
                   (current-publish-port (option-ref options 'publish-port "8080")))
      (verify-cluster (car args))
      (verify-port (current-publish-port))
      (container-cluster-create (car args) (option-ref options 'servers 1)))))

(define remove-options-spec
  `((force (single-char #\f) (value #f))))
(define (remove-cluster . args)
  (let* ((options (if (null? args) '() (getopt-long args remove-options-spec)))
         (args (option-ref options '() '())))
    (parameterize ((force? (option-ref options 'force #f)))
      (container-cluster-remove (car args)))))

(define worker-options-spec
  '())
(define (add-worker-to-clusters node-def)
  (let ((worker-name (sxml-query "name" node-def))
        (cluster-name (sxml-query "cluster" node-def)))
    (container-cluster-add-worker node-def)
    ;; NOTE: cluster-name is its own network name.
    (dns-update worker-name cluster-name)))

(define *help-content*
  "
Usage: alexon cluster subcommand [options]

Subcommands:
  list
  create
  remove
  worker
")

(define (show-help . _)
  (display
   (string-append announce-head
                  *help-content*
                  announce-foot)))

(define (show-all-cmds)
  (format #t "~{~a~^ ~}" '(list create remove worker)))

(define (handle-cluster . args)
  "Manipulate cluster operations."
  (match args
    (()
     (show-help))
    (("--cmd-list")
     (show-all-cmds))
    (("list" "--list-options")
     (display (option-spec->str list-options-spec)))
    (("list" . _)
     (apply list-clusters args))
    (("create" "--list-options")
     (display (option-spec->str create-options-spec)))
    (("create" . _)
     (apply create-cluster args))
    (("remove" "--list-options")
     (display (option-spec->str remove-options-spec)))
    (("remove" . _)
     (apply remove-cluster args))
    (("worker" "--list-options")
     (display (option-spec->str worker-options-spec)))
    (("worker" . _)
     (match args
       ((_ worker-manifest)
        (add-worker-to-clusters (read-worker-manifest worker-manifest)))
       (else (display "Usage: alexon cluster worker manifest.(json|yaml)\n"))))
    (else
     (show-help))))

(define %summary "Manipulate container clusters.")
(define main handle-cluster)
