;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon commands node)
  #:use-module (alexon utils)
  #:use-module (alexon commands)
  #:use-module (alexon container)
  #:use-module (ice-9 getopt-long)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (json))

(define list-options-spec
  `((json (value #f))))
(define (list-node . args)
  (let* ((options (if (null? args) '() (getopt-long args list-options-spec)))
         (args (option-ref options '() '()))
         (nodes (container-node-list)))
    (parameterize ((just-output-json? (option-ref options 'json #f)))
      (cond
       ((zero? (vector-length nodes))
        (display "No node found.\n"))
       ((just-output-json?)
        (print-json-info nodes))
       (else
        (format #t "ID~/NAME~3/ROLE~2/CLUSTER~/  STATE~2/ADDR~2/START~%")
        (for-each
         (lambda (i)
           (when (equal? (sxml-query "Labels/app" (vector-ref nodes i)) "alexon")
             (let* ((node (vector-ref nodes i))
                    (id (substring/shared (assoc-ref node "Id") 0 6))
                    (name (node->name node))
                    (role (sxml-query "Labels/alexon.role" node))
                    (cluster (sxml-query "Labels/alexon.cluster" node))
                    (state (sxml-query "State" node))
                    (network-name (sxml-query "Labels/alexon.cluster.network" node))
                    (network-namespace
                     (format #f "NetworkSettings/Networks/~a/IPAddress" network-name))
                    (addr (or (sxml-query network-namespace node)
                              "(Stopped)"))
                    (start (strftime "%Y-%m-%d %T" (localtime (sxml-query "Created" node)))))
               (format #t "~6a~/~20a~/~15a~/~8a  ~10a~/~10a~/~10a~%"
                       id name role cluster state addr start))))
         (iota (vector-length nodes))))))))

(define create-options-spec
  `((from-image (value #t))
    (from-container (value #t))
    (from-volume (value #t))
    (role (value #t))
    (cluster (value #t))))
(define (create-node . args)
  (cond
   ((null? args)
    (error "Please specify node name."))
   (else
    ;; handle options
    (let* ((options (if (null? args) '() (getopt-long args create-options-spec)))
           (args (option-ref options '() '())))
      (parameterize ((current-image-name (option-ref options 'from-image #f))
                     (current-container-name (option-ref options 'from-container #f))
                     (current-volume-name (option-ref options 'from-volume #f))
                     (current-role (option-ref options 'role "worker"))
                     (current-cluster (option-ref options 'cluster "default")))
        (let ((id (container-node-create (car args))))
          (format #t "~a~%" id)))))))

(define remove-options-spec
  `((force (single-char #\f) (value #f))))
(define (remove-node . args)
  (match args
    (("remove")
     (error "Please specify node name."))
    (else
     ;; handle options
     (let* ((options (if (null? args) '() (getopt-long args remove-options-spec)))
            (args (option-ref options '() '())))
       (parameterize ((force? (assoc-ref options 'force)))
         (format #t "Node ~a removed.~%" (container-node-remove (car args))))))))

(define (update-node . args)
  #t)

(define inspect-options-spec
  `())
(define (inspect-node . args)
  ;; inspect node, just print json
  (cond
   ((null? args)
    (error "Please specify node name."))
   (else
    (let ((node (container-node-inspect (car args) #:quiet? #t)))
      (cond
       (node
        (display (scm->json-string node))
        (newline))
       (else
        (error "Node ~a not found." (car args))))))))

(define *help-content*
  "
    Usage: alexon node subcommand [options]

    Subcommands:
    list
    create
    remove
    update
    inspect
    ")

(define (show-help . _)
  (display
   (string-append announce-head
                  *help-content*
                  announce-foot)))

(define (show-all-cmds)
  (format #t "~{~a~^ ~}" '(list create remove update inspect)))

(define (handle-node . args)
  "Manipulate nodes operations."
  (match args
    (()
     (show-help))
    (("--cmd-list")
     (show-all-cmds))
    (("list" "--list-options")
     (display (option-spec->str list-options-spec)))
    (("list" . _)
     (apply list-node args))
    (("create" "--list-options")
     (display (option-spec->str create-options-spec)))
    (("create" . _)
     (apply create-node args))
    (("remove" "--list-options")
     (display (option-spec->str remove-options-spec)))
    (("remove" . _)
     (apply remove-node args))
    (("update" . _)
     (apply update-node args))
    #;
    (("update" "--update-options")    ;
    (display (option-spec->str update-option-spec)))
    (("inspect" "--inspect-options")
     (display (option-spec->str inspect-options-spec)))
    (("inspect" . _)
     (apply inspect-node (cdr args)))
    (else
     (show-help))))

(define %summary "Manipulate nodes.")
(define main handle-node)
