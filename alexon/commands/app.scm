;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon commands app)
  #:use-module (alexon utils)
  #:use-module (alexon commands)
  #:use-module (alexon container)
  #:use-module (alexon manifest)
  #:use-module (alexon app)
  #:use-module (ice-9 getopt-long)
  #:use-module (ice-9 format)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 match)
  #:use-module (json))

(define list-options-spec
  `((json (value #f))))
(define (list-app . args)
  (let* ((options (if (null? args) '() (getopt-long args list-options-spec)))
         (args (option-ref options '() '()))
         (nodes (container-node-list)))
    (cond
     ((zero? (vector-length nodes))
      (display "No app found.\n"))
     (else
      (format #t "NAME~2/START~%")
      (let ((app-names (make-hash-table)))
        (for-each
         (lambda (i)
           (when (equal? (sxml-query "Labels/app" (vector-ref nodes i)) "alexon")
             (hash-set! app-names (sxml-query "Labels/alexon.app.name" (vector-ref nodes i))
                        (sxml-query "State/Running" (vector-ref nodes i)))))
         (iota (vector-length nodes)))
        (hash-for-each (lambda (k v)
                         (format #t "~a~4/~a~%" k v))
                       app-names))))))

(define create-options-spec
  `())
(define (create-app file)
  (let ((manifest (read-manifest-file file)))
    (create-app-with-manifest manifest)))

(define remove-options-spec
  `((force (single-char #\f) (value #f))))
(define (remove-app . args)
  (let* ((options (if (null? args) '() (getopt-long args remove-options-spec)))
         (args (option-ref options '() '()))
         (manifest (read-manifest-file (car args)))
         (app-name (sxml-query "name" manifest)))
    (parameterize ((force? (option-ref options 'force #f)))
      (container-cluster-remove app-name))))

(define inspect-options-spec
  `())
(define (inspect-app . args)
  (error "Alexon app inspect haven't implemented yet."))

(define start-options-spec
  `())
(define (start-app . args)
  (error "Alexon app start haven't implemented yet."))

(define stop-options-spec
  `())
(define (stop-app . args)
  (error "Alexon app stop haven't implemented yet."))


(define *help-content*
  "
Usage: alexon app subcommand [options]

Subcommands:
  list
  create
  start
  stop
  remove
  inspect
")

(define (show-help . _)
  (display
   (string-append announce-head
                  *help-content*
                  announce-foot)))

(define (show-all-cmds)
  (format #t "~{~a~^ ~}" '(list create remove)))

(define (handle-app . args)
  "Manipulate app operations."
  (match args
    (()
     (show-help))
    (("--cmd-list")
     (show-all-cmds))
    (("list" "--list-options")
     (display (option-spec->str list-options-spec)))
    (("list" . _)
     (apply list-app args))
    (("create" "--list-options")
     (display (option-spec->str create-options-spec)))
    (("create" . _)
     (match args
       ((_ manifest-file)
        (create-app manifest-file))
       (else
        (format (current-error-port) "alexon app create manifest.(json|yaml)~%")
        (exit -11))))
    (("remove" "--list-options")
     (display (option-spec->str remove-options-spec)))
    (("remove" . _)
     (apply remove-app args))
    (("start" "--list-options")
     (display (option-spec->str start-options-spec)))
    (("start" . _)
     (apply start-app args))
    (("stop" "--list-options")
     (display (option-spec->str stop-options-spec)))
    (("stop" . _)
     (apply stop-app args))
    (("inspect" "--inspect-options")
     (display (option-spec->str inspect-options-spec)))
    (("inspect" . _)
     (apply inspect-app (cdr args)))
    (else
     (show-help))))

(define %summary "Manipulate Cloud Native Applications.")
(define main handle-app)
