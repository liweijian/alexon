;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon utils)
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 control)
  #:use-module (ice-9 format)
  #:use-module (ice-9 match)
  #:use-module (web client)
  #:use-module (json)
  #:use-module (srfi srfi-1)
  #:use-module ((rnrs) #:select (get-bytevector-n
                                 get-string-all
                                 bytevector->u8-list
                                 div
                                 get-u8))
  #:export (gen-random-token
            mktemp
            remove-ext
            %current-toplevel
            find-ENTRY-path
            sxml-query
            sxml-replace!
            unit-test::sxml-query
            alist-set!
            unit-test::alist-set!
            run-it-with-catch
            unix-socket-get
            unix-socket-post
            unix-socket-put
            unix-socket-delete
            print-json-info
            just-output-json?

            ;; node params
            current-cluster
            current-image-name
            current-container-name
            current-volume-name
            current-volume-driver
            current-volume-size
            current-role
            current-publish-port

            ;; network params
            current-network-driver
            current-network-gateway
            current-network-subnet
            current-network-subnet-gateway
            current-network-ip-range

            force?
            specified-docker-host
            get-proper-subnet-ip
            info->gateway
            info->name
            node->name
            get-fix-script-path
            target-fix-script-path
            *default-fix-script-name*
            must-specified

            ;; checkers
            list-checker
            string-checker
            number-checker
            boolean-checker
            vector-checker
            string->list/checker
            make-general-verifier

            fill-config-with-defaults
            gen-docker-env
            read-json-file
            is-socket-port-occupied?
            ))

(define* (gen-random-token #:key (length 8) (uppercase #f))
  (define-syntax-rule (gen-for-even port)
    (let ((bv (get-bytevector-n port (div length 2))))
      (format #f "~{~2,'0x~}" (bytevector->u8-list bv))))

  (call-with-input-file "/dev/urandom"
    (lambda (port)
      ;; generate token directly when length is even, otherwise, add the last byte with 8bit length.
      (and=>
       (cond
        ((even? length) (gen-for-even port))
        (else
         (string-append (gen-for-even port)
                        (format #f "~x" (logand #xf (get-u8 port))))))
       (lambda (str)
         (if uppercase
             (string-upcase str)
             str))))))

(define* (mktemp #:key (pattern "/tmp/tmp-XXXXXX") (mode "w+"))
  (let ((port (mkstemp! (string-copy pattern) mode)))
    (chmod port (logand #o666 (lognot (umask))))
    port))

(define (remove-ext str)
  (let ((i (string-contains str ".")))
    (substring str 0 i)))

(define *alexon-entry* ".ENTRY")

(define %current-toplevel (make-parameter #f))

(define* (find-ENTRY-path proc #:optional (check-only? #f))
  (define (-> p)
    (let ((ff (string-append p "/" *alexon-entry*)))
      (and (file-exists? ff) p)))
  (define (last-path pwd)
    (and=> (string-match "(.*)/.*$" pwd) (lambda (m) (match:substring m 1))))
  ;; FIXME: Maybe we should generate relative path?
  (let lp((pwd (getcwd)))
    (cond
     ((not (string? pwd)) (error find-ENTRY-path "BUG: please report it!" pwd))
     ((string-null? pwd)
      (if check-only?
          #f
          (error find-ENTRY-path
                 "No ENTRY! Are you in a legal Alexon app dir? Or maybe you need to create a new app?")))
     ((-> pwd) => proc)
     (else (lp (last-path pwd))))))

(define-syntax-rule (sxml-query pattern alst)
  (let/ec return
    (fold
     (lambda (x p)
       (or (assoc-ref p x)
           (return #f)))
     alst
     (string-split pattern #\/))))

(define (sxml-replace! alst key val)
  (define (list-deep-copy lst)
    (map (lambda (x) (if (list? x) (list-deep-copy x) x)) lst))
  (let ((new-lst (list-deep-copy alst))
        (v (assoc-ref alst key)))
    (cond
     ((null? v)
      (assoc-set! new-lst key val))
     (else
      (assoc-set! new-lst key (cons val v))))))

;; unit test for sxml-query, which is used to parse the assoc-list.
;; function name: unit-test::sxml-query
;; parameter 1: pattern as string
;; parameter 2: alist as assoc-list
;; parameter 3: test value as any type
;; return: boolean
(define (unit-test::sxml-query pattern alist test-value)
  (let* ((result (sxml-query pattern alist)))
    (equal? result test-value)))

(define* (alist-set! alst ks v #:optional (clone? #t))
  "A functional version of assoc-set! Set the value of key namespace `ks' in alist `alst' to `v'."
  (let ((new-lst (if #:clone? (list-copy alst) alst)))
    (let lp ((ks ks) (rest new-lst))
      (cond
       ((assoc-ref rest (car ks))
        => (lambda (vv)
             (cond
              ((null? (cdr ks))
               (assoc-set! rest (car ks) v)
               alst)
              (else (lp (cdr ks) vv)))))
       (else (throw 'alexon-error alist-set!
                    "Invalid key namespace `~a'!" ks))))))

;; unit test for alist-set!
;; function name: unit-test::alist-set!
;; parameter 1: alist as assoc-list
;; parameter 2: key namespace as list
;; parameter 3: value as any type
;; parameter 4: test value as any type
;; return: boolean
(define (unit-test::alist-set! alist ks v test-value)
  (let* ((result (alist-set! alist ks v)))
    (equal? result test-value)))

(define-syntax run-it-with-catch
  (syntax-rules ()
    ((_ #:error-handler error-handler body ...)
     (catch #t
       (lambda () body ...)
       error-handler))
    ((_ body ...)
     (catch #t
       (lambda () body ...)
       (lambda (k . e)
         (error (format #f "[ERROR] ~a" e)))))))

(define (connect-to-unix-socket socket-path)
  (let ((sock-addr (make-socket-address AF_UNIX socket-path))
        (sock (socket PF_UNIX SOCK_STREAM 0)))
    (setsockopt sock SOL_SOCKET SO_REUSEADDR 1)
    (connect sock sock-addr)
    sock))

(define (make-unix-socket-method method)
  (lambda* (socket-path path #:key (headers '()) (body '()))
    (catch #t
      (lambda ()
        (let ((sock (connect-to-unix-socket socket-path)))
          (case method
            ((GET) (http-get path #:port sock #:headers headers))
            ((POST) (http-post path #:port sock #:headers headers #:body body))
            ((PUT) (http-put path #:port sock #:headers headers #:body body))
            ((DELETE) (http-delete path #:port sock #:headers headers))
            (else
             (error 'alexon-error make-unix-socket-method "Invalid method `~a'!" method)))))
      (lambda (k . e)
        (error (format #f "connect-to-unix-socket: ~s ~a" socket-path e))))))

(define unix-socket-get (make-unix-socket-method 'GET))
(define unix-socket-post (make-unix-socket-method 'POST))
(define unix-socket-put (make-unix-socket-method 'PUT))
(define unix-socket-delete (make-unix-socket-method 'DELETE))

(define* (print-json-info info #:key (is-list? #f))
  (let* ((info (if is-list? info (vector->list info)))
         (info (filter (lambda (i)
                         (equal? (sxml-query "Labels/app" i) "alexon"))
                       info))
         (info (list->vector info)))
    (scm->json info #:pretty #t)
    (newline)))

(define just-output-json? (make-parameter #f))

(define current-cluster (make-parameter "default"))
(define current-image-name (make-parameter #f))
(define current-container-name (make-parameter #f))
(define current-volume-name (make-parameter #f))
(define current-volume-driver (make-parameter "local"))
(define current-volume-size (make-parameter #f))
(define current-role (make-parameter #f))
(define current-publish-port (make-parameter #f))
(define force? (make-parameter #f))
(define specified-docker-host (make-parameter #f))

;; Network params
(define current-network-driver (make-parameter "bridge"))
(define current-network-subnet (make-parameter #f))
(define current-network-gateway (make-parameter #f))
(define current-network-dns (make-parameter #f))
(define current-network-ip-range (make-parameter #f))

(define (get-proper-subnet-ip ip-lst)
  (cond
   ((null? ip-lst) "168.1.0.0/16")
   (else
    (let* ((nearest-subnet (car (sort ip-lst string-ci>=?)))
           (seg (string-split nearest-subnet #\.)))
      (match seg
        ((a b c d) ; for example, ("168" x "0" "1/16")
         (let ((new (1+ (string->number b))))
           (cond
            ;; The default subnet range in Alexon is from 168.1.0.0/16 to 168.168.0.0/16
            ((> new 168)
             (error
              "Too many networks created! The max subnet is up to 168.168.0.1/16. Try to remove some networks if you don't use them."))
            (else (string-join (list a (object->string new) c d) ".")))))
        (else
         (error (format #f "Invalid subnet ip `~a'!" nearest-subnet))))))))

;; info->gateway
(define (info->gateway info)
  (let* ((network-name (sxml-query "Config/Labels/alexon.cluster.network" info))
         (namespace (format #f "NetworkSettings/Networks/~a/Gateway" network-name)))
    (sxml-query namespace info)))

;; NOTE: node->name is for the info returned by node-inspect
;;       the Name is a string here, by Docker API.
(define (info->name info)
  (substring/shared (sxml-query "Name" info) 1))

;; NOTE: node->name is for the info returned by node-list
;;       the Names is a vector here, by Docker API.
(define (node->name node)
  (match (sxml-query "Names" node)
    (#(name) (substring/shared name 1))
    (else "")))

(define *default-fix-script-name* "alexon-node-fix.sh")

(define (get-fix-script-path)
  (let ((path (format #f "~a/etc/alexon/scripts/~a"
                      (or (getenv "ALEXON_INSTALL_PATH") "")
                      *default-fix-script-name*)))
    (if (file-exists? path)
        path
        (error "`~a' is missing!" path))))

(define (target-fix-script-path)
  (format #f "/bin/~a" *default-fix-script-name*))

(define (must-specified key)
  (lambda (value)
    (or value
        (error (format #f "must specified ~a" key)))))

(define (make-field-checker key type checker?)
  (lambda (x)
    (if (checker? x)
        x
        (error (format #f "Invalid type of `~a', the field `~a' must be a ~a!"
                       x key type)))))

(define (list-checker key)
  (make-field-checker key "list" list?))

(define (string-checker key)
  (make-field-checker key "string" string?))

(define (vector-checker key)
  (make-field-checker key "vector" vector?))

(define (number-checker key)
  (make-field-checker key "number" number?))

(define (boolean-checker key)
  (make-field-checker key "boolean" boolean?))

(define (string->list/checker key)
  (lambda (x)
    (if (string? x)
        ((make-field-checker key "string" list?) (string-split x #\sp))
        (error (format #f "Invalid type of `~a', the field `~a' must be a string!"
                       x key)))))

(define (make-general-verifier type lst)
  (lambda (node-def)
    (for-each (lambda (item)
                (match item
                  ((key checker default)
                   (cond
                    ((assoc-ref node-def key)
                     => (lambda (value)
                          (when (not (checker value))
                            (error
                             (format #f "~a-verify: invalid value for key ~a: ~a"
                                     type key value)))))
                    (else #t)))))
              lst)))

(define (fill-config-with-defaults proc-name spec scm)
  (map (lambda (p)
         (match p
           ((key checker default)
            (cond
             ((assoc-ref scm key)
              => (lambda (v) (cons key v)))
             (else (cons key default))))
           (else (error proc-name "Invalid key-value `~a'!" p))))
       spec))

(define (gen-docker-env scm)
  (list->vector
   (map (lambda (p)
          (match p
            ((key . value)
             (format #f "~a=~a" key value))
            (else (error (format #f "gen-docker-env: Invalid key-value `~a'!" p)))))
        scm)))

(define (read-json-file path)
  (json-string->scm (call-with-input-file path get-string-all)))

(define (is-socket-port-occupied? socket-port)
  (when (not (number? socket-port))
    (error "BUG: Invalid socket port `~a', have you converted it to number?" socket-port))
  (let ((s (socket PF_INET SOCK_STREAM 0)))
    (catch #t
      (lambda ()
        (bind s AF_INET INADDR_ANY socket-port)
        (close s)
        #f)
      (lambda args
        (close s)
        #t))))
