;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon app)
  #:use-module (alexon utils)
  #:use-module (alexon worker)
  #:use-module (alexon verify)
  #:use-module (alexon dns)
  #:use-module (alexon manifest)
  #:use-module (alexon container)
  #:use-module (srfi srfi-1)
  #:use-module (json)
  #:export (create-app-with-manifest))

(define (node-def? node)
  (sxml-query "nodeDef" node))

(define (node-file? node)
  (sxml-query "nodeFile/path" node))

(define (node-verify manifest)
  (verify-worker worker-verifier manifest))

(define (nodes-checker name)
  (lambda (nodes)
    (when (not (list? nodes))
      (error (format #f "nodes must be a list: ~a" nodes)))
    (for-each
     (lambda (node)
       (cond
        ((node-def? (list node)) => node-verify)
        ((node-file? (list node))
         => (lambda (path)
              (cond
               ((and (string? path) (file-exists? path)) #t)
               (else
                (error (format #f "nodeFile/path must be a valid file path: ~a" path))))))
        (else
         (error
          (format #f "node must be a nodeDef or a nodeFile, but got ~a" node)))))
     nodes)))

(define *default-app-values*
  `(("name" ,(must-specified "name") #f)
    ("port" ,(string-checker "port") 1234)
    ("mode" ,(string-checker "mode") "web2")
    ("nodes" ,(nodes-checker "nodes") ())
    ))

(define app-verifier (make-general-verifier 'app *default-app-values*))

(define (path->node-def path)
  (let ((manifest (read-worker-manifest path)))
    (verify-manifest worker-verifier manifest)
    (or manifest
        (error path "Worker manifest must have a nodes section"))))

(define (get-all-nodes-def nodes)
  (fold (lambda (node p)
          (cond
           ((node-def? (list node))
            (cons node p))
           ((node-file? (list node))
            => (lambda (path)
                 (cons (path->node-def path) p)))
           (else p)))
        '()
        nodes))

(define* (get-nodes/type manifest #:optional (filter 'all))
  (define checker
    (cond
     ((eq? filter 'all)
      (lambda (node-def) #t))
     ((string? filter)
      (lambda (node-def)
        (string=? filter (sxml-query "type" node-def))))
     ((procedure? filter) filter)
     (else (error get-nodes/type "filter must be a string or a procedure" filter))))
  (let ((nodes (get-all-nodes-def (sxml-query "nodes" manifest))))
    (fold (lambda (node-def p)
            (if (checker node-def)
                (cons node-def p)
                p))
          '()
          nodes)))

(define (create-app-with-manifest manifest)
  (verify-manifest app-verifier manifest)
  (parameterize ((current-cluster (sxml-query "name" manifest))
                 (current-publish-port (sxml-query "port" manifest)))
    (verify-cluster (current-cluster))
    (verify-port (current-publish-port))
    (let ((workers (map worker-constructor (get-nodes/type manifest "worker"))))
      (container-cluster-create (current-cluster) 1)
      (for-each
       (lambda (worker)
         (let ((worker-name (sxml-query "name" worker)))
           (container-cluster-add-worker worker)
           (dns-update worker-name (current-cluster))))
       workers))))
