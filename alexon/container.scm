;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon container)
  #:use-module (alexon utils)
  #:use-module (alexon docker)
  #:use-module (alexon dns)
  #:use-module ((rnrs) #:select (define-record-type))
  #:export (current-container

            container-node-list
            container-node-create
            container-node-remove
            container-node-inspect
            container-node-run-cmd
            container-node-upload-file-and-run

            container-network-create
            container-network-list
            container-network-remove
            container-network-nodes-list
            container-network-inspect

            container-cluster-list
            container-cluster-create
            container-cluster-remove
            container-cluster-inspect
            container-cluster-update
            container-cluster-add-worker
            ))

(define-record-type <container>
  (fields
   node-list
   node-create
   node-remove
   node-inspect
   node-run-cmd
   node-upload-file-and-run
   network-list
   network-remove
   network-create
   network-connect
   network-nodes-list
   network-inspect
   cluster-list
   cluster-create
   cluster-remove
   cluster-inspect
   cluster-update
   cluster-add-worker
   ))

(define-record-type <docker> (parent <container>))

;; TODO: We haven't supported the containerd yet.
(define-record-type <containerd> (parent <container>))

(define (make-docker)
  (make-<docker>
   docker-node-list
   docker-node-create
   docker-node-remove
   docker-node-inspect
   docker-node-run-cmd
   docker-node-upload-file-and-run
   docker-network-list
   docker-network-remove
   docker-network-create
   docker-network-connect
   docker-network-nodes-list
   docker-network-inspect
   docker-cluster-list
   docker-cluster-create
   docker-cluster-remove
   docker-cluster-inspect
   docker-cluster-update
   docker-cluster-add-worker
   ))

(define current-container (make-parameter (make-docker)))

;; Note all containers creation use the same interface, say, the container may
;; not have base image as docker does. So we're not going to design the interface with these
;; parameters. The specific options should be parameterized.
(define (container-node-create name)
  ((<container>-node-create (current-container)) name))

(define (container-node-remove name/id)
  ((<container>-node-remove (current-container)) name/id))

(define (container-node-list)
  ((<container>-node-list (current-container))))

(define* (container-node-inspect name/id #:key (quiet? #t))
  ((<container>-node-inspect (current-container)) name/id #:quiet? quiet?))

(define (container-node-run-cmd name/id cmd)
  ((<container>-node-run-cmd (current-container)) name/id cmd))

(define (container-node-upload-file-and-run name/id file path cmd)
  ((<container>-node-upload-file-and-run (current-container))
   name/id file path cmd))

(define (container-network-list)
  ((<container>-network-list (current-container))))

(define (container-network-remove name)
  ((<container>-network-remove (current-container)) name))

(define (container-network-create name)
  ((<container>-network-create (current-container)) name))

(define (container-network-connect node-name network-id/name)
  ((<container>-network-connect (current-container)) node-name network-id/name))

(define (container-network-nodes-list network-name)
  ((<container>-network-nodes-list (current-container)) network-name))

(define* (container-network-inspect network-name #:key (quiet? #t))
  ((<container>-network-inspect (current-container)) network-name #:quiet? quiet?))

(define (container-cluster-list)
  ((<container>-cluster-list (current-container))))

(define (container-cluster-create name cnt)
  ((<container>-cluster-create (current-container)) name cnt))

(define (container-cluster-remove name)
  ((<container>-cluster-remove (current-container)) name))

(define* (container-cluster-inspect name #:key (quiet? #t))
  ((<container>-cluster-inspect (current-container)) name #:quiet? quiet?))

(define (container-cluster-update name)
  ((<container>-cluster-update (current-container)) name))

(define (container-cluster-add-worker node-def)
  ((<container>-cluster-add-worker (current-container)) node-def))
