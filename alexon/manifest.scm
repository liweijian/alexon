;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon manifest)
  #:use-module (alexon utils)
  #:use-module (alexon verify)
  #:use-module (alexon yaml)
  #:use-module (ice-9 match)
  #:use-module (ice-9 format)
  #:use-module (ice-9 regex)
  #:use-module (json)
  #:export (read-manifest-file))

(define *default-app-values*
  `(("apiVersion" . ,verify-api-version)
    ("kind" . ,verify-kind)
    ))

(define (top-verify manifest)
  (for-each
   (lambda (p)
     (match p
       ((k . checker)
        (cond
         ((assoc-ref manifest k) => checker)
         (else
          (error (format #f "missing item `~a' in manifest file!" k)))))
       (else (error (format #f "unknown item `~a' in manifest file!" p)))))
   *default-app-values*)
  manifest)

;; The constructor and verifier should be in each kind module. We just read manifest
;; file here.
(define (read-manifest-file file)
  (top-verify
   (cond
    ((string-match ".(yaml|yml)$" file)
     (yaml-file->scm file))
    ((string-match ".json$" file)
     (read-json-file file))
    (else (error (format #f "read-worker-file: unknown file type `~a'!" file))))))
