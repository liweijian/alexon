;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon dns)
  #:use-module (alexon utils)
  #:use-module (alexon yaml)
  #:use-module (alexon container)
  #:use-module (ice-9 match)
  #:export (dns-update))

;; In default, we use CoreDNS in container.
(define current-dns-manifest
  (make-parameter "/etc/alexon/data/coredns.yaml"))

;; generate host file for dns, the nodes are get from docker-network-nodes-list
(define (gen-hosts-file network-name)
  (define (fix ip)
    (substring/shared ip 0 (- (string-length ip) 3)))
  (let ((nodes (container-network-nodes-list network-name)))
    (with-output-to-string
      (lambda ()
        (for-each
         (lambda (node)
           (format #t "~a ~a~%"
                   (sxml-query "Name" node)
                   (fix (sxml-query "IPv4Address" node))))
         nodes)))))

(define (read-dns-manifest)
  (let ((manifest (current-dns-manifest)))
    (yaml-file->scm manifest)))

(define (inject-dns-manifest network-name m)
  (match m
    ((service-account cluster-role cluster-role-binding config-map deployment kube-dns)
     (let ((hosts (gen-hosts-file network-name)))
       (scm->yaml-string
        (sxml-replace! config-map "data" (cons "NodeHosts" hosts)))))
    (else (error "inject-dns-manifest: dns manifest format error" m))))

(define (dns-update node-name network-name)
  (let* ((yaml-string (inject-dns-manifest network-name (read-dns-manifest)))
         (port (mktemp #:pattern "/tmp/coredns.yaml.XXXXXX"))
         (filename (port-filename port)))
    (display yaml-string port)
    (close port)
    (rename-file filename "/tmp/coredns.yaml")
    (container-node-upload-file-and-run node-name
                                        "/tmp/coredns.yaml"
                                        "/"
                                        `("/bin/k3s" "kubectl"
                                          "apply" "-f" ,filename))))
