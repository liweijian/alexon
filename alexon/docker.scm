;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon docker)
  #:use-module (alexon utils)
  #:use-module (alexon docker node)
  #:use-module (alexon docker network)
  #:use-module (alexon docker cluster)
  #:use-module (alexon docker worker)
  #:use-module (alexon docker loadbalancer)
  #:re-export (;; Docker node
               docker-node-list
               docker-node-create
               docker-node-remove
               docker-node-inspect
               docker-node-run-cmd
               docker-node-upload-file-and-run

               ;; Docker network
               docker-network-list
               docker-network-create
               docker-network-remove
               docker-network-connect
               is-docker-network-exist?
               docker-network-gateway-ip
               docker-network-nodes-list
               docker-network-inspect

               ;; Docker worker
               docker-worker-create

               ;; Docker load balancer
               docker-loadbalancer-create

               ;; Docker cluster
               docker-cluster-create
               docker-cluster-remove
               docker-cluster-list
               docker-cluster-inspect
               docker-cluster-update
               docker-cluster-add-worker
               ))
