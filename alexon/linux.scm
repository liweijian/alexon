;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon linux)
  #:use-module (alexon cli)
  #:use-module ((rnrs) #:select (get-bytevector-all))
  #:export (get-local-ip
            get-raw-file-content
            uncompress
            gzip-compress
            bzip2-compress
            xz-compress
            get-k3s-path))

(define (get-local-ip)
  (string-trim-both
   (<cli>-result
    (cli-run*  ip addr show "`ip -4 route show default | cut -d \" \" -f5`"
               "| grep 'inet ' | awk '{print $2}' | cut -f1 -d'/' | cut -f2 -d' '"))))

(define (get-raw-file-content path)
  (call-with-input-file path get-bytevector-all))

(define (uncompress path)
  (let ((zf (format #f "/tmp/~a.tar" (basename path))))
    (cli-run* tar cf ,zf ,path)
    (get-raw-file-content zf)))

(define (gzip-compress path)
  (let ((zf (format #f "/tmp/~a.tar.gz" (basename path))))
    (cli-run* tar Pczf ,zf ,path)
    (get-raw-file-content zf)))

(define (bzip2-compress path)
  (let ((zf (format #f "/tmp/~a.tar.bz2" (basename path))))
    (cli-run* tar Pcjf ,zf ,path)
    (get-raw-file-content zf)))

(define (xz-compress path)
  (error "xz-compress not implemented yet"))

(define (get-k3s-path)
  (let ((result (cli-run* "which" "k3s")))
    (if (zero? (<cli>-status result))
        (string-trim-both (<cli>-result result))
        (error "k3s not found, have you installed it?"))))
