;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon docker volume)
  #:use-module (alexon utils)
  #:use-module (web response)
  #:use-module (alexon docker api)
  #:use-module (json)
  #:use-module ((rnrs) #:select (utf8->string))
  #:export (docker-volume-create
            docker-volume-remove))

(define (verify-volume-params)
  (when (string=? "local" (current-volume-driver))
    (cond
     ((current-volume-size)
      (error "When the volume driver is \"local\", the size shouldn't be specified!")))))

;; if name is #f, docker will generate a random name
(define* (docker-volume-create name #:key (opts '()) (driver "local")
                               (labels '()))
  (verify-volume-params)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-post
          (current-docker)
          (docker-volume-create-api name)
          #:headers '((content-type application/json))
          #:body (scm->json-string `(,@(if name `(("Name" . ,name)) '())
                                     ("Driver" . ,driver)
                                     ("DriverOpts" . ,opts)
                                     ("Labels" . (("app" . "alexon")
                                                  ,@labels))))))
     (lambda (response body)
       (let ((json (json-string->scm (utf8->string body))))
         (cond
          ((= 201 (response-code response))
           (assoc-ref json "Name"))
          (else
           (error (format #f "[ERROR ~a] ~a"
                          (response-code response)
                          (assoc-ref json "message"))))))))))

(define (docker-volume-remove id/name)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-delete
          (current-docker)
          (docker-volume-remove-api id/name)))
     (lambda (response body)
       (cond
        ((= 204 (response-code response)) #t)
        (else
         (let ((json (json-string->scm (utf8->string body))))
           (error (format #f "[ERROR ~a] ~a"
                          (response-code response)
                          (assoc-ref json "message"))))))))))
