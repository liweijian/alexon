;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon docker worker)
  #:use-module (alexon utils)
  #:use-module (alexon docker defaults)
  #:use-module (alexon docker node)
  #:use-module (ice-9 format)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 match)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11)
  #:export (docker-worker-create
            get-all-workers-exported-ports))

;; TODO:
;; 1. load template from file.
;; 2. parse the template and run the sub-script to create the worker config.
;; 3. create the worker node with the config.

;; parse manifest json and call node creater to create the worker node.
(define (docker-worker-create cluster-info node-def)
  (define (parse-ports ports)
    (let ((ports (vector->list ports)))
      (let lp ((ports ports) (bind-list '()) (publish-list '()))
        (cond
         ((null? ports) (values bind-list publish-list))
         (else
          (let ((port (car ports)))
            (let* ((name (sxml-query "name" port))
                   (bind (sxml-query "bind" port))
                   (protocol (or (sxml-query "protocol" port) "tcp"))
                   (port-map (string-split bind #\:))
                   (inside (car port-map))
                   (outside (cadr port-map)))
              (lp (cdr ports)
                  (cons (cons (format #f "~a/~a" inside protocol)
                              `#((("HostPort" . ,outside))))
                        bind-list)
                  (cons (list (format #f "~a/~a" outside protocol)) publish-list)))))))))
  (let* ((worker-name (sxml-query "name" node-def))
         (working-dir (sxml-query "workdir" node-def))
         (cmd (sxml-query "cmd" node-def))
         (entrypoint (sxml-query "entrypoint" node-def))
         (volume-name (format #f "alexon-worker-~a-volume" worker-name))
         (volume-bind (format #f "~a:/alexon/volume:z" volume-name))
         (image (sxml-query "image" node-def))
         (cluster-name (sxml-query "cluster" node-def))
         (network-name cluster-name)
         (server-ip (docker-node-ip (info->name cluster-info)))
         (token (sxml-query "Config/Labels/alexon.cluster.token" cluster-info))
         (gateway-ip (info->gateway cluster-info)))
    (let-values (((binds exports) (parse-ports (sxml-query "ports" node-def))))
      (docker-node-create
       worker-name
       #:image image
       #:role "worker"
       #:entrypoint entrypoint
       #:cmd (string-split cmd #\sp)
       #:working-dir working-dir
       #:host-config `(("Privileged" . #t)
                       ("PortBindings" ,@binds))
       #:exposed-ports exports
       #:labels `(("alexon.role" . "worker")
                  ("alexon.cluster" . ,cluster-name)
                  ("alexon.cluster.token" . ,token)
                  ("alexon.cluster.network" . ,network-name))
       #:binds `(,volume-bind ,@(cluster-node-default-binds))
       #:volumes `((,volume-name))
       #:env `(("ALEXON_GATEWAY" . ,gateway-ip)))

      (display "Running fix script on the worker node......")
      (docker-node-upload-file-and-run worker-name
                                       (get-fix-script-path)
                                       "/"
                                       `("/bin/sh" ,(target-fix-script-path) "all"))
      (display "done.\n"))))

(define (get-all-cluster-workers-info cluster-name)
  (let ((nodes (vector->list (docker-node-list))))
    (fold (lambda (node prev)
            (cond
             ((and (equal? "worker" (sxml-query "Labels/alexon.role" node))
                   (equal? cluster-name (sxml-query "Labels/alexon.cluster" node)))
              (cons node prev))
             (else prev)))
          '()
          nodes)))

(define (get-all-workers-exported-ports cluster-name)
  (let ((workers (get-all-cluster-workers-info cluster-name)))
    (map (lambda (worker)
           (let ((name (node->name worker))
                 (ports (vector->list (sxml-query "Ports" worker))))
             (cons name
                   (fold (lambda (port-info prev)
                           (let ((port (sxml-query "PrivatePort" port-info))
                                 (type (sxml-query "Type" port-info)))
                             (if port
                                 (cons (format #f "~a.~a" port type) prev)
                                 prev)))
                         '()
                         ports))))
         workers)))
