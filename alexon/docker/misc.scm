;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon docker misc)
  #:use-module (alexon utils)
  #:use-module (alexon docker api)
  #:use-module (json)
  #:use-module ((rnrs) #:select (utf8->string))
  #:use-module (web response)
  #:export (docker-client-info
            docker-root-dir))

(define (docker-client-info)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-get
          (current-docker)
          (docker-info-api)
          #:headers '((content-type application/json))))
     (lambda (response body)
       (let ((json (json-string->scm (utf8->string body))))
         (cond
          ((= 200 (response-code response)) json)
          (else
           (error
            (format #f "[ERROR ~a] ~a"
                    (response-code response)
                    (assoc-ref json "message"))))))))))

(define (docker-root-dir)
  (let ((info (docker-client-info)))
    (sxml-query "DockerRootDir" info)))
