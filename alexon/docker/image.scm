;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon docker image)
  #:use-module (alexon utils)
  #:use-module (alexon docker api)
  #:use-module ((rnrs) #:select (utf8->string))
  #:use-module (json)
  #:use-module (web response)
  #:use-module (web uri)
  #:export (dock-image-pull
            docker-image-remove
            docker-image-inspect
            check-image-and-pull
            docker-image-exists?))

(define (docker-image-pull image-name)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-post
          (current-docker)
          (docker-image-pull-api image-name)
          #:headers '((content-type application/json))
          #:body "{}"))
     (lambda (response body)
       (cond
        ((= 200 (response-code response)) #t)
        ((= 404 (response-code response)) #f)
        (else
         (let ((json (json-string->scm (utf8->string body))))
           (error
            (format #f "[ERROR ~a] ~a"
                    (response-code response)
                    (assoc-ref json "message"))))))))))

(define (docker-image-inspect name/id)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-get
          (current-docker)
          (docker-image-inspect-api name/id)))
     (lambda (response body)
       (let ((json (json-string->scm (utf8->string body))))
         (cond
          ((= 200 (response-code response)) json)
          ((= 404 (response-code response)) #f)
          (else
           (error
            (format #f "[ERROR ~a] ~a"
                    (response-code response)
                    (assoc-ref json "message"))))))))))

(define (docker-image-exists? name)
  (docker-image-inspect name))

(define (check-image-and-pull image-name)
  (when (not (docker-image-exists? image-name))
    (format #t "Pulling image ~a......~%" image-name)
    (cond
     ((docker-image-pull image-name)
      (display "done~%"))
     (else
      (error (format #f "Failed to pull image ~a" image-name))))))
