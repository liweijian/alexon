;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon docker network)
  #:use-module (alexon utils)
  #:use-module (alexon docker api)
  #:use-module (web response)
  #:use-module (json)
  #:use-module ((rnrs) #:select (utf8->string))
  #:use-module (srfi srfi-1)
  #:use-module (ice-9 match)
  #:export (docker-network-list
            docker-network-create
            docker-network-remove
            docker-network-connect
            is-docker-network-exist?
            docker-network-gateway-ip
            docker-network-nodes-list
            docker-network-inspect))

(define (get-network-config network-id)
  (let ((network-info (docker-network-inspect network-id)))
    (vector-ref (sxml-query "IPAM/Config" network-info) 0)))

(define (docker-network-list)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-get
          (current-docker)
          (docker-network-list-api (scm->json-string '(("label" . #("app=alexon")))))
          #:headers '((content-type application/json))))
     (lambda (response body)
       (let ((json (json-string->scm (utf8->string body))))
         (cond
          ((= 200 (response-code response)) json)
          (else
           (error (format #f "[ERROR ~a] ~a" (response-code response)
                          (assoc-ref json "message"))))))))))

(define (get-network-from-cluster cluster-name)
  (let ((networks (vector->list (docker-network-list))))
    (any (lambda (net)
           (and (equal? cluster-name (assoc-ref net "Name"))
                net))
         networks)))

(define (network->subnet network-info)
  (let ((config (get-network-config network-info)))
    (sxml-query "Subnet" config)))

(define (docker-network-create name)
  (define (detect-subnet)
    (cond
     ((current-network-subnet) => identity)
     (else
      (let ((ip-list (filter-map network->subnet (vector->list (docker-network-list)))))
        (get-proper-subnet-ip ip-list)))))
  (define (gen-gateway-ip subnet)
    (match (string-split subnet #\.)
      ((a b c d)
       (let* ((p (string-split d #\/))
              (new-d (1+ (string->number (car p)))))
         (format #f "~a.~a.~a.~a" a b c new-d)))))
  (define (detect-gateway subnet)
    (cond
     ((current-network-gateway) `(("Gateway" . ,(current-network-gateway))))
     (else `(("Gateway" . ,(gen-gateway-ip subnet))))))
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (let ((subnet (detect-subnet)))
           (unix-socket-post
            (current-docker)
            (docker-network-create-api)
            #:headers '((content-type application/json))
            #:body (scm->json-string `(("Name" . ,name)
                                       ("Driver" . ,(current-network-driver))
                                       ("IPAM" .
                                        (("Config" .
                                          #((("Subnet" . ,subnet)
                                             ,@(detect-gateway subnet))))))
                                       ("Options" .
                                        (("com.docker.network.bridge.name" . ,name)))
                                       ("Labels" . (("app" . "alexon"))))))))
     (lambda (response body)
       ;; check code 200, other raise error and print message
       (let ((json (json-string->scm (utf8->string body))))
         (if (= 201 (response-code response))
             (assoc-ref json "Id")
             (error (format #f "[ERROR ~a] ~a" (response-code response)
                            (assoc-ref json "message")))))))))

(define (docker-network-remove name/id)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-delete
          (current-docker)
          (docker-network-remove-api name/id)
          #:headers '((content-type application/json))))
     (lambda (response body)
       ;; check code 200, other raise error and print message
       (cond
        ((= 204 (response-code response)) #t)
        ((= 404 (response-code response)) #f)
        (else
         (let ((json (json-string->scm (utf8->string body))))
           (error (format #f "[ERROR ~a] ~a"
                          (response-code response)
                          (assoc-ref json "message"))))))))))

(define (docker-network-connect container-name network-id)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-post
          (current-docker)
          (docker-network-connect-api network-id)
          #:headers '((content-type application/json))
          #:body (scm->json-string `(("Container" . ,container-name)))))
     (lambda (response body)
       (cond
        ((= 200 (response-code response)) #t)
        (else
         (error
          (format #f "[ERROR ~a] ~a"
                  (response-code response)
                  (assoc-ref (json-string->scm (utf8->string body)) "message")))))))))

(define (docker-network-inspect network-id)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-get
          (current-docker)
          (docker-network-inspect-api network-id)
          #:headers '((content-type application/json))))
     (lambda (response body)
       ;; check code 200, other raise error and print message
       (let ((json (json-string->scm (utf8->string body))))
         (if (= 200 (response-code response))
             json
             (error (format #f "[ERROR ~a] ~a"
                            (response-code response)
                            (assoc-ref json "message")))))))))

(define (is-docker-network-exist? name)
  (catch #t
    (lambda ()
      (docker-network-inspect name)
      #t)
    (lambda e
      #f)))

(define (docker-network-gateway-ip network-id)
  (let ((config (get-network-config network-id)))
    (sxml-query "Gateway" config)))

(define (docker-network-nodes-list network-id)
  (let ((network-info (docker-network-inspect network-id)))
    (sxml-query "Containers" network-info)))
