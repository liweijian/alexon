;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon docker api)
  #:use-module (alexon utils)
  #:use-module (web uri)
  #:use-module (ice-9 regex)
  #:export (current-docker
            current-docker-version

            docker-info-api

            docker-node-list-api
            docker-node-create-api
            docker-node-start-api
            docker-node-inspect-api
            docker-node-remove-api
            docker-node-update-api
            docker-node-exec-api
            docker-node-exec-start-api
            docker-node-archive-api
            docker-node-stop-api

            docker-network-list-api
            docker-network-remove-api
            docker-network-create-api
            docker-network-connect-api
            docker-network-inspect-api

            docker-volume-create-api
            docker-volume-remove-api

            docker-image-pull-api
            docker-image-inspect-api
            docker-image-remove-api))

(define %current-docker (make-parameter "/run/docker.sock"))
(define (current-docker)
  (let ((host (or (getenv "DOCKER_HOST")
                  (specified-docker-host)
                  (%current-docker))))
    (cond
     ((string-match "containerd" host)
      (error "Containerd hasnt't been supported yet!"))
     (else host))))

(define current-docker-version (make-parameter "v1.43"))

(define (gen-api api-url)
  (format #f "http+unix://localhost/~a/~a" (current-docker-version) api-url))

(define (docker-info-api)
  (gen-api "info"))

;; node api
(define (docker-node-list-api json-string)
  (gen-api (format #f "containers/json?all=true&filters=~a" (uri-encode json-string))))
(define (docker-node-create-api name)
  (gen-api (format #f "containers/create?name=~a" name)))
(define (docker-node-start-api id) (gen-api (format #f "containers/~a/start" id)))
(define (docker-node-inspect-api name/id)
  (gen-api (format #f "containers/~a/json" name/id)))
(define (docker-node-remove-api name/id force?)
  (gen-api (format #f "containers/~a?force=~a" name/id force?)))
(define (docker-node-update-api name/id)
  (gen-api (format #f "containers/~a/update" name/id)))
(define (docker-node-exec-api name/id) (gen-api (format #f "containers/~a/exec" name/id)))
(define (docker-node-exec-start-api id) (gen-api (format #f "exec/~a/start" id)))
(define* (docker-node-archive-api id/name path #:key (overwrite? #f) (copy-uid-gid? #t))
  (gen-api (format #f "containers/~a/archive?path=~a&noOverwriteDirNonDir=~a&copyUIDGID=~a"
                   id/name (uri-encode path)
                   (if overwrite? 'false 'true)
                   (if copy-uid-gid? 'true 'false))))
(define (docker-node-stop-api id timeout)
  (gen-api (format #f "containers/~a/stop?t=~a" id timeout)))

;; network api
(define (docker-network-list-api json-string)
  (gen-api (format #f "networks?filters=~a" (uri-encode json-string))))
(define (docker-network-remove-api id) (gen-api (format #f "networks/~a" id)))
(define (docker-network-create-api) (gen-api "networks/create"))
(define (docker-network-connect-api network-id/name)
  (gen-api (format #f "networks/~a/connect" network-id/name)))
(define (docker-network-inspect-api network-id/name)
  (gen-api (format #f "networks/~a" network-id/name)))

;; volume api
(define (docker-volume-create-api name)
  (gen-api (format #f "volumes/create?name=~a" name)))
(define (docker-volume-remove-api id/name)
  (gen-api (format #f "volumes/~a" id/name)))

;; image api
(define (docker-image-pull-api image-name)
  (gen-api (format #f "images/create?fromImage=~a" (uri-encode image-name))))
(define (docker-image-inspect-api id/name)
  (gen-api (format #f "images/~a/json" id/name)))
(define (docker-image-remove-api id/name force?)
  (gen-api (format #f "images/~a?force=~a" id/name force?)))
