;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon docker loadbalancer)
  #:use-module (alexon utils)
  #:use-module (alexon yaml)
  #:use-module (alexon docker defaults)
  #:use-module (alexon docker node)
  #:use-module (alexon docker worker)
  #:use-module (alexon docker network)
  #:use-module (ice-9 format)
  #:use-module (ice-9 regex)
  #:use-module (srfi srfi-1)
  #:export (update-loadbalancer-config
            docker-loadbalancer-create))

(define (get-loadbalancer-info cluster-name)
  (let ((nodes (vector->list (docker-node-list))))
    (or (any (lambda (node)
               (cond
                ((and (equal? "loadbalancer" (sxml-query "Labels/alexon.role" node))
                      (equal? cluster-name (sxml-query "Labels/alexon.cluster" node)))
                 (if (string-match (format #f "alexon-~a-loadbalancer" cluster-name)
                                   (node->name node))
                     node
                     #f))
                (else #f)))
             nodes)
        (error (format #f "No loadbalancer found for cluster ~a" cluster-name)))))

(define (merge-ports-info pinfo)
  (let ((ht (make-hash-table)))
    (for-each
     (lambda (info)
       (let ((name (car info))
             (ports (cdr info)))
         (for-each (lambda (port)
                     (hash-set! ht port
                                (cons name (hash-ref ht port '()))))
                   ports)))
     pinfo)
    (hash-map->list
     (lambda (port names)
       (cons port (list->vector names)))
     ht)))

;; inject loadblancer config with new portmap.
(define (inject-loadbalance-config lb-name cluster-name)
  (let* ((config (default-loadbalance-config))
         (ports (merge-ports-info (get-all-workers-exported-ports cluster-name)))
         (port (mktemp #:pattern "/tmp/port-map.yaml-XXXXXX"))
         (filename (port-filename port))
         (scm (sxml-replace! config "ports" ports)))
    (display (scm->yaml-string scm) port)
    (close port)
    filename))

(define (update-loadbalancer-config lb-name cluster-name)
  (let ((config-file (inject-loadbalance-config lb-name cluster-name)))
    ;; We don't need to run confd here, since it's working in watching mode.
    ;; So we just need to update the config file.
    (rename-file config-file "/tmp/port-map.yaml")
    (docker-node-upload-file-and-run lb-name
                                     "/tmp/port-map.yaml"
                                     "/"
                                     '("/bin/run-nginx.sh"))))

;; Create loadbalancer, each cluster has only 1 loadbalancer.
;; The loadbalancer is a container which runs on the docker, and it is used to forward the
;; request to the worker nodes.
(define (docker-loadbalancer-create cluster-name network-name)
  (let ((lb-name (format #f "alexon-~a-loadbalancer" cluster-name))
        (publish-port (current-publish-port)))
    (docker-node-create lb-name
                        #:image "registry.gitlab.com/symecloud/lb-proxy:latest"
                        #:role "loadbalancer"
                        #:exposed-ports '(("443/tcp"))
                        #:host-config `(("PortBindings"
                                         ("443/tcp" .
                                          #((("HostPort" . ,publish-port))))))
                        #:labels `(("app" . "alexon")
                                   ("alexon.role" . "loadbalancer")
                                   ("alexon.cluster" . ,cluster-name)
                                   ("alexon.cluster.network" . ,network-name)))
    (docker-network-connect lb-name network-name)
    (docker-node-run-cmd lb-name '("/bin/run-nginx.sh"))))
