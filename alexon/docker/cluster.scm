;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon docker cluster)
  #:use-module (alexon utils)
  #:use-module (alexon docker node)
  #:use-module (alexon docker network)
  #:use-module (alexon docker volume)
  #:use-module (alexon docker misc)
  #:use-module (alexon docker defaults)
  #:use-module (alexon docker worker)
  #:use-module (alexon docker loadbalancer)
  #:use-module (alexon linux)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 pretty-print)
  #:use-module (srfi srfi-1)
  #:export (get-cluster-token
            docker-cluster-create
            docker-cluster-remove
            docker-cluster-inspect
            docker-cluster-update
            docker-cluster-list
            docker-cluster-add-worker
            get-all-cluster-servers-info
            gen-custom-token-for-cluster))

(define (get-all-cluster-servers-info name)
  (let ((nodes (vector->list (docker-node-list))))
    (fold (lambda (node prev)
            (cond
             ((and (equal? "server" (sxml-query "Labels/alexon.role" node))
                   (equal? name (sxml-query "Labels/alexon.cluster" node)))
              (if (string-match (format #f "alexon-~a-server-*" name)
                                (node->name node))
                  (cons node prev)
                  prev))
             (else prev)))
          '()
          nodes)))

(define (get-all-cluster-nodes-info name)
  (let ((nodes (vector->list (docker-node-list))))
    (fold (lambda (node prev)
            (cond
             ((equal? name (sxml-query "Labels/alexon.cluster" node))
              (cons node prev))
             (else prev)))
          '()
          nodes)))

(define* (docker-cluster-inspect cluster-name)
  (let ((servers (get-all-cluster-servers-info cluster-name)))
    (car (map (lambda (node)
                (let ((node-name (node->name node)))
                  (docker-node-inspect node-name)))
              servers))))

(define (gen-custom-token-for-cluster)
  (gen-random-token #:length 20))

(define (get-cluster-token cluster-name)
  (let ((server (docker-cluster-inspect cluster-name)))
    (cond
     (server
      (sxml-query "Labels/alexon.cluster.token" server))
     (else
      (error (format #f "get-cluster-token: Cluster ~a not found." cluster-name))))))

(define (docker-cluster-list)
  (let ((nodes (vector->list (docker-node-list))))
    (fold (lambda (node prev)
            (cond
             ((equal? "server" (sxml-query "Labels/alexon.role" node))
              (cons node prev))
             (else prev)))
          '()
          nodes)))

(define (create-server-node cluster-name server-name volume-name network-name)
  (let* ((token (gen-custom-token-for-cluster))
         (volume-bind (format #f "~a:/alexon/volume:z" volume-name))
         (gateway-ip (docker-network-gateway-ip network-name)))
    ;; create server node
    (format #t "Creating ~a server node......" server-name)
    (docker-node-create
     server-name
     #:image (default-k3s-image)
     #:role "server"
     #:entrypoint ""
     #:cmd '("tail" "-f" "/dev/null")
     #:host-config `(("Privileged" . #t)
                     ("PortBindings" ("9999/tcp" .
                                      #((("HostPort" . ,(default-api-port)))))))
     #:exposed-ports `((,(format #f "~a/tcp" (default-api-port))))
     #:labels `(("alexon.role" . "server")
                ("alexon.cluster" . ,cluster-name)
                ("alexon.cluster.token" . ,token)
                ("alexon.cluster.network" . ,network-name))
     #:binds `(,volume-bind ,@(cluster-node-default-binds))
     #:volumes `((,volume-name))
     #:env `(("ALEXON_GATEWAY_IP" . ,gateway-ip)))

    (display "done.\n")

    ;; run fix script on the server node to make sure everything is ok.
    (display "Running fix script on the server node......")
    (docker-node-upload-file-and-run server-name
                                     (get-fix-script-path)
                                     "/"
                                     `("/bin/sh" ,(target-fix-script-path) "all"))
    (display "done.\n")

    ;; run k3s server
    (display "Starting cluster's Kubernetes server......")
    (docker-node-run-cmd server-name
                         `("/bin/k3s" "server"
                           "--cluster-init"
                           "--token" ,token))
    (display "done.\n")

    (format #t "Add ~a to cluster network......" server-name)
    (docker-network-connect server-name network-name)
    (display "done.\n")))

(define (docker-cluster-create name cnt)
  (define network-name name)
  (define (gen-volume-path volume-name)
    (format #f "~a/volumes/~a" (docker-root-dir) volume-name))
  (let ((network-name name)  ; The network name keeps the same with the cluster name.
        (volume-name (format #f "alexon-~a-volume" name)))
    (display "Creating cluster network......")
    (docker-network-create network-name)
    (display "done.\n")

    (display "Creating cluster volume......")
    (docker-volume-create volume-name
                          #:driver (current-volume-driver)
                          #:opts '()
                          #:labels `(("app" . "alexon")
                                     ("alexon.cluster" . ,name)))
    (display "done.\n")

    (for-each (lambda (i)
                (let ((server-name (format #f "alexon-~a-server-~a" name i)))
                  (create-server-node name server-name volume-name network-name)))
              (iota cnt))

    (display "Creating cluster loadbalancer......")
    (docker-loadbalancer-create name network-name)
    (display "done.\n")))

(define (docker-cluster-remove cluster-name)
  (define (mounts-remove server)
    (let ((mounts (vector->list (sxml-query "Mounts" server))))
      (for-each (lambda (volume)
                  (let ((volume-name (sxml-query "Name" volume)))
                    (when volume-name
                      (format #t "Removing volume ~a......" volume-name)
                      (docker-volume-remove volume-name)
                      (display "done.\n"))))
                mounts)))
  (let ((nodes (get-all-cluster-nodes-info cluster-name)))
    (for-each (lambda (node)
                (let ((node-name (node->name node)))
                  (when (and (is-docker-node-running? node-name) (force?))
                    (format #t "Node ~a is still running, try to stop it......" node-name)
                    (docker-node-stop node-name #:quiet? #t)
                    (display "done.\n"))
                  (docker-node-remove node-name)
                  (mounts-remove node)))
              nodes)
    (when (is-docker-network-exist? cluster-name)
      (format #t "Removing cluster network ~a......" cluster-name)
      (docker-network-remove cluster-name)
      (display "done.\n"))))

(define (get-cluster-token cluster-name)
  (let ((servers (get-all-cluster-servers-info cluster-name)))
    (if (null? servers)
        (error "get-cluster-token: no server node found.")
        (let ((server (car servers)))
          (sxml-query "Labels/alexon.cluster.token" server)))))

;; extract useful info from manifest
;; create worker node
;; add worker to the cluster
;; upload k3s to the worker
(define (docker-cluster-add-worker node-def)
  (let* ((worker-name (sxml-query "name" node-def))
         (cmd (sxml-query "cmd" node-def))
         (cluster-name (sxml-query "cluster" node-def))
         (lb-name (format #f "alexon-~a-loadbalancer" cluster-name))
         (cluster-info (docker-cluster-inspect cluster-name))
         (cluster-token (get-cluster-token cluster-name))
         (network-name (sxml-query "Config/Labels/alexon.cluster.network" cluster-info))
         (worker (docker-worker-create cluster-info node-def)))

    ;; add worker to the cluster
    (display "Adding worker to the cluster......")
    (docker-network-connect worker-name network-name)
    (display "done.\n")

    (display "Update loadbalancer config......")
    (update-loadbalancer-config lb-name cluster-name)
    (display "done.\n")

    ;; upload k3s to the worker, and add woker node to k3s
    (display "Schedule worker node with Kubernetes......")
    (docker-node-upload-file-and-run worker-name
                                     (get-k3s-path)
                                     "/"
                                     `("/bin/k3s" "agent" "--server"
                                       ,(format #f "https://~a:~a"
                                                cluster-name (default-api-port))
                                       "--token" ,cluster-token))
    (display "done.\n")))

(define (docker-cluster-update name configs)
  #t)
