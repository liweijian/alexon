;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon docker node)
  #:use-module (alexon utils)
  #:use-module (alexon docker api)
  #:use-module (alexon docker image)
  #:use-module (alexon docker defaults)
  #:use-module (alexon linux)
  #:use-module (json)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-11) ; for let-values
  #:use-module ((rnrs) #:select (utf8->string))
  #:use-module (web response)
  #:use-module (ice-9 match)
  #:export (docker-node-start
            docker-node-update
            docker-node-list
            docker-node-create
            docker-node-remove
            docker-node-inspect
            docker-node-exec
            docker-node-exec-start
            docker-node-ip
            docker-node-receive-file
            docker-node-run-cmd
            docker-node-upload-file-and-run
            docker-node-stop
            is-docker-node-running?
            node->id))

(define (docker-node-start id)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-post
          (current-docker)
          (docker-node-start-api id)
          #:headers '((content-type application/json))
          #:body "{}"))
     (lambda (response body)
       (cond
        ((= 204 (response-code response)) #t)
        (else
         (let ((json (json-string->scm (utf8->string body))))
           (error
            (format #f "[ERROR ~a] ~a"
                    (response-code response)
                    (assoc-ref json "message"))))))))))

(define (docker-node-update name/id configs)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-post
          (current-docker)
          (docker-node-update-api name/id)
          #:headers '((content-type application/json))
          #:body (scm->json-string configs)))
     (lambda (response body)
       (let ((json (json-string->scm (utf8->string body))))
         (cond
          ((= 200 (response-code response)) (assoc-ref json "Id"))
          (else
           (error
            (format #f "[ERROR ~a] ~a"
                    (response-code response)
                    (assoc-ref json "message"))))))))))

(define* (docker-node-create name #:key (role "nomad")
                             (image (default-k3s-image))
                             (entrypoint #f)
                             (cmd '(""))
                             (working-dir "/")
                             (network-config '())
                             (exposed-ports '(("80/tcp")))
                             (host-config '())
                             (publish-all-ports? #f)
                             (labels '())
                             (configs '())
                             (binds '())
                             (volumes '())
                             (env '()))
  (let ((image-name (or (current-image-name) image))
        (the-role (or (current-role) role)))
    (check-image-and-pull image-name)
    (run-it-with-catch
     (call-with-values
         (lambda ()
           (unix-socket-post
            (current-docker)
            (docker-node-create-api name)
            #:headers '((content-type application/json))
            #:body (scm->json-string
                    `(("Image" . ,image-name)
                      ,@(if entrypoint
                            `(("Entrypoint" . #(,entrypoint)))
                            '())
                      ("Cmd" . #(,@cmd))
                      ("WorkingDir" . ,working-dir)
                      ("Labels" . (("app" . "alexon")
                                   ("alexon.role" . ,the-role)
                                   ,@labels))
                      ("ExposedPorts" . ,exposed-ports)
                      ("HostConfig"
                       ,@host-config
                       ("Binds" . #(,@binds))
                       ("Volumes" . ,volumes)
                       ("PublishAllPorts" . ,publish-all-ports?))
                      ("ENV" . ,(gen-docker-env env))
                      ,@configs))))
       (lambda (response body)
         (let ((json (json-string->scm (utf8->string body))))
           (cond
            ((= 201 (response-code response))
             (let ((id (assoc-ref json "Id")))
               (display "starting the node...")
               (docker-node-start id)
               (display "ok...")
               id))
            (else
             (error (format #f "[ERROR ~a] ~a"
                            (response-code response)
                            (assoc-ref json "message")))))))))))

(define (docker-node-remove name/id)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-delete
          (current-docker)
          (docker-node-remove-api name/id (if (force?) "true" "false"))
          #:headers '((content-type application/json))))
     (lambda (response body)
       ;; check response code
       (if (= 204 (response-code response))
           name/id
           (error (format #f "[ERROR ~a] ~a"
                          (response-code response)
                          (utf8->string body))))))))

(define (docker-node-list)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-get
          (current-docker)
          (docker-node-list-api (scm->json-string '(("label" . #("app=alexon")))))))
     (lambda (response body)
       (let ((json (json-string->scm (utf8->string body))))
         (cond
          ((= 200 (response-code response)) json)
          (else
           (error
            (format #f "[ERROR ~a] ~a"
                    (response-code response)
                    (assoc-ref json "message"))))))))))

(define* (docker-node-inspect name/id #:key (quiet? #f))
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-get
          (current-docker)
          (docker-node-inspect-api name/id)
          #:headers '((content-type application/json))))
     (lambda (response body)
       (cond
        ((= 200 (response-code response))
         (json-string->scm (utf8->string body)))
        ((and (= 404 (response-code response)) quiet?) #f)
        (else
         (error
          (format #f "[ERROR ~a] ~a"
                  (response-code response)
                  (assoc-ref (json-string->scm (utf8->string body)) "message")))))))))

(define (docker-node-ip name/id)
  (let* ((scm (docker-node-inspect name/id))
         (network-name (sxml-query "Config/Labels/alexon.cluster.network" scm))
         (namespace (format #f "NetworkSettings/Networks/~a/IPAddress" network-name)))
    (sxml-query namespace scm)))

(define* (docker-node-exec name cmd #:key (working-dir "/"))
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-post
          (current-docker)
          (docker-node-exec-api name)
          #:headers '((content-type application/json))
          #:body (scm->json-string `(("AttachStdin" . #f)
                                     ("AttachStdout" . #t)
                                     ("AttachStderr" . #t)
                                     ("WorkingDir" . ,working-dir)
                                     ("Cmd" . #(,@cmd))))))
     (lambda (response body)
       (let ((json (json-string->scm (utf8->string body))))
         (cond
          ((= 201 (response-code response)) (assoc-ref json "Id"))
          (else
           (error
            (format #f "[ERROR ~a] ~a"
                    (response-code response)
                    (assoc-ref (json-string->scm json) "message"))))))))))

(define (docker-node-exec-start id)
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-post
          (current-docker)
          (docker-node-exec-start-api id)
          #:headers '((content-type application/json))
          #:body (scm->json-string `(("Detach" . #f)
                                     ("Tty" . #f)))))
     (lambda (response body)
       (cond
        ((= 200 (response-code response)) #t)
        (else
         (error
          (format #f "[ERROR ~a] ~a"
                  (response-code response)
                  (assoc-ref (json-string->scm (utf8->string body)) "message")))))))))

(define* (docker-node-receive-file node-name file-path target-path #:key (type "bzip2")
                                   (overwrite? #t))
  (define (type->converter type)
    (match type
      ((or 'raw "raw") (values 'application/octet-stream (uncompress file-path)))
      ((or 'gzip "gzip") (values 'application/x-gz (gzip-compress file-path)))
      ((or 'bzip2 "bzip2") (values 'application/x-bz2 (bzip2-compress file-path)))
      ((or 'xz "xz") (values 'application/x-xz (xz-compress file-path)))
      (else (error (format #f "[ERROR] docker-node-receive-file: unknown type ~a" type)))))
  (let-values (((mime body) (type->converter type)))
    (run-it-with-catch
     (call-with-values
         (lambda ()
           (unix-socket-put
            (current-docker)
            (docker-node-archive-api node-name target-path #:overwrite? overwrite?)
            #:headers `((content-type ,mime))
            #:body body))
       (lambda (response body)
         (cond
          ((= 200 (response-code response)) #t)
          (else
           (error
            (format #f "[ERROR ~a] ~a"
                    (response-code response)
                    (assoc-ref (json-string->scm (utf8->string body)) "message"))))))))))

(define* (docker-node-run-cmd node-name cmd #:key (working-dir "/"))
  (let ((id (docker-node-exec node-name cmd #:working-dir working-dir)))
    (docker-node-exec-start id)))

(define* (docker-node-upload-file-and-run node-name file-path target-path cmd
                                          #:key (type "bzip2") (working-dir "/")
                                          (overwrite? #t))
  (docker-node-receive-file node-name file-path target-path #:type type
                            #:overwrite? overwrite?)
  (docker-node-run-cmd node-name cmd #:working-dir working-dir))

(define* (docker-node-stop name/id #:key (timeout 10) (quiet? #f))
  (run-it-with-catch
   (call-with-values
       (lambda ()
         (unix-socket-post
          (current-docker)
          (docker-node-stop-api name/id timeout)
          #:headers '((content-type application/json))
          #:body ""))
     (lambda (response body)
       (cond
        ((= 204 (response-code response)) #t)
        ((= 304 (response-code response)) #f)
        ((and quiet? (= 404 (response-code response))) #f)
        (else
         (let ((json (json-string->scm (utf8->string body))))
           (error
            (format #f "[ERROR ~a] ~a"
                    (response-code response)
                    (assoc-ref json "message"))))))))))

(define (is-docker-node-running? name)
  (let ((json (docker-node-inspect name #:quiet? #t)))
    (and json
         (string=? "running" (sxml-query "State/Status" json)))))

(define (node->id name)
  (let ((json (docker-node-inspect name)))
    (sxml-query "Id" json)))
