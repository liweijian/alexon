;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(define-module (alexon docker defaults)
  #:export (cluster-node-default-binds
            default-k3s-image
            default-dns-image
            default-api-port
            default-loadbalance-config))

(define (cluster-node-default-binds)
  '("/etc/alexon:/root"
    "/var/run/cni:/var/run/cni"
    "/var/lib/rancher/k3s:/var/lib/rancher/k3s"
    "/var/lib/kubelet:/var/lib/kubelet"
    "/var/log:/var/log"))

(define default-k3s-image (make-parameter "rancher/k3s:v1.27.2-rc3-k3s1"))

(define default-dns-image (make-parameter "coredns/coredns:1.10.1"))

(define default-api-port (make-parameter "6443"))

(define default-loadbalance-config (make-parameter
                                    `(("settings"
                                       ("workerConnections" . 1080)
                                       ("defaultProxyTimeout" . 600))
                                      ("ports"))))
