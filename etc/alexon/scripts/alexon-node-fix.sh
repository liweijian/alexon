#!/bin/sh

# This script was inspired by k3d's entrypoint script

set -o errexit
set -o nounset

# fix a potential issue with cgroup v2 where the root cgroup does not have the necessary
# controllers enabled, which can cause issues with resource management and container
# isolation.
function fix_cgroup() {

    if [ -f /sys/fs/cgroup/cgroup.controllers ]; then
        echo "[$(date -Iseconds)] [CgroupV2 Fix] Evacuating Root Cgroup ..."
        # move the processes from the root group to the /init group,
        # otherwise writing subtree_control fails with EBUSY.
        mkdir -p /sys/fs/cgroup/init
        busybox xargs -rn1 < /sys/fs/cgroup/cgroup.procs > /sys/fs/cgroup/init/cgroup.procs || :
        # enable controllers
        sed -e 's/ / +/g' -e 's/^/+/' <"/sys/fs/cgroup/cgroup.controllers" >"/sys/fs/cgroup/cgroup.subtree_control"
        echo "[$(date -Iseconds)] [CgroupV2 Fix] Done"
    fi

    echo "[$(date -Iseconds)] Running fix_cgroup"  >> "$LOGFILE"
    "fix_cgroup"  >> "$LOGFILE" 2>&1 || exit 1
}

# fix potential issues with DNS resolution when using Docker and Alexon together by updating
# the iptables rules and /etc/resolv.conf file to use the gateway IP address instead of
# Docker's embedded DNS server.
function fix_dns() {
    docker_dns="127.0.0.11" # docker embedded DNS
    gateway="$ALEXON_GATEWAY_IP"

    echo "[$(date -Iseconds)] [DNS Fix] Use the detected Gateway IP $gateway instead of Docker's embedded DNS ($docker_dns)"

    # Change iptables rules added by Docker to route traffic to out Gateway IP instead of
    # Docker's embedded DNS.
    #
    # Modifies the iptables rules to use the gateway IP address instead of Docker's
    # embedded DNS server. This command uses iptables-save to export the current iptables
    # rules, pipes the output to sed to replace the Docker DNS IP address with the gateway
    # IP address, and then pipes the modified output to iptables-restore to apply the
    # updated rules.
    echo "[$(date -Iseconds)] [DNS Fix] > Changing iptables rules ..."
    iptables-save \
        | sed \
              -e "s/-d ${docker_dns}/-d ${gateway}/g" \
              -e 's/-A OUTPUT \(.*\) -j DOCKER_OUTPUT/\0\n-A PREROUTING \1 -j DOCKER_OUTPUT/' \
              -e "s/--to-source :53/--to-source ${gateway}:53/g"\
        | iptables-restore

    # Update resolv.conf to use the Gateway IP if needed: this will also make CoreDNS use it via k3s' default `forward . /etc/resolv.conf` rule in the CoreDNS config
    grep -q "${docker_dns}" /etc/resolv.conf
    grepstatus=$?
    if test $grepstatus -eq 0; then
        echo "[$(date -Iseconds)] [DNS Fix] > Replacing IP in /etc/resolv.conf ..."
        cp /etc/resolv.conf /etc/resolv.conf.original
        sed -e "s/${docker_dns}/${gateway}/g" /etc/resolv.conf.original >/etc/resolv.conf
    fi

    echo "[$(date -Iseconds)] Running fix_dns"  >> "$LOGFILE"
    "fix_dns"  >> "$LOGFILE" 2>&1 || exit 1
}

function fix_mount() {
    echo "[$(date -Iseconds)] [Mounts] Fixing Mountpoints..."
    # Recursively mark all mount points as shared. This ensures that any mounts created in
    # child mount namespaces are propagated to the parent mount namespace, and vice versa.
    #
    # This can be useful in containerized environments where multiple containers share the
    # same host filesystem and need to access the same mounts.
    mount --make-rshared /

    echo "[$(date -Iseconds)] Running fix_mount"  >> "$LOGFILE"
    "fix_mount"  >> "$LOGFILE" 2>&1 || exit 1
}



######################
# ENTRY

LOGFILE="/var/log/alexon-fix-script_$(date "+%y%m%d%H%M%S").log"

touch "$LOGFILE"

exec /bin/k3s server --tls-san 0.0.0.0 &
k3s_pid=$!

echo "[$(date -Iseconds)] Running Alexon node fix script..." >> "$LOGFILE"
# the program entry to detect the arguments, dns for fix_dns, cgroup for fix_cgroup
while getopts $1; do
    case $opt in
        dns)
            fix_dns
            ;;
        cgroup)
            fix_cgroup
            ;;
        mount)
            fix_mount
            ;;
        all)
            fix_dns
            fix_cgroup
            fix_mount
            ;;
        test)
            echo "test"
            ;;
        *)
            echo "The valid options are: dns, cgroup"
            ;;
    esac
done
echo "[$(date -Iseconds)] Finished Alexon node fix scripts!" >> "$LOGFILE"


# shellcheck disable=SC3028
until kubectl uncordon "$HOSTNAME"; do sleep 3; done

# shellcheck disable=SC3028
cleanup() {
    echo Draining node...
    kubectl drain "$HOSTNAME" --force --delete-emptydir-data
    echo Sending SIGTERM to k3s...
    kill -15 $k3s_pid
    echo Waiting for k3s to close...
    wait $k3s_pid
    echo Bye!
}

# shellcheck disable=SC3048
trap cleanup SIGTERM SIGINT SIGQUIT SIGHUP

wait $k3s_pid
echo "Alexon cleanup!"
