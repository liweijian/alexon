#!/bin/bash

function create_mariadb() {
    sudo kubectl apply -f web/pv.yaml
    sudo kubectl apply -f web/db-pv-claim.yaml
    sudo kubectl apply -f web/mariadb-db-deployment.yaml
    sudo kubectl apply -f web/mariadb-service.yaml
    sudo kubectl apply -f web/mariadb-config.yaml
}

function show_db_preparation() {
    sudo kubectl get pv
    sudo kubectl get pvc
    sudo kubectl get deployments
    sudo kubectl get services
    sudo kubectl get configmap
    sudo kubectl describe configmap mariadb-config
}

function create_secrets() {
    rm -fr /tmp/secrets
    mkdir /tmp/secrets
    echo -n 'random_secret_for_the_backend' > /tmp/secrets/backend_secret.txt
    sudo kubectl create secret generic backend-secrets --from-file=backend_secret=/tmp/secrets/backend_secret.txt
}
