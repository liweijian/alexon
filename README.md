<center><img src="https://alexon.dev/img/alexon.png" alt="Alexon" width="330"></center>
<center>

# Alexon - the super easy tool for distributed operating system

Main site: [alexon.dev](https://alexon.dev)

## Introduction

Alexon aims the be the foundation tool of a cloud native distributed OS.

Alexon was written with GNU Guile, which is known for its support of functional programming concepts. As a result, Alexon is influenced by the functional programming paradigm

Alexon simplified the deployment of the cloud native application. You don't have to be expert to deal with DNS, loadbalancer, Kubernetes by yourself, just concentrate on your goals.

## How to build

### Dependencies

```bash
# assume you're in Debian/Ubuntu
sudo apt install docker-ce guile-3.0 guile-3.0-dev make autoconf automake libtool pkgconf bash util-linux curl
```
Or you may follow [Docker official manual](https://docs.docker.com/engine/install/).

Then install necessary dependencies:
```bash
# In the current alexon source directory
sudo ./scripts/install-deps.sh --no-docker # since you've already installed docker
```

And make sure the docker works:
```bash
docker info
```

### Build

```bash
./configure
make
make install
```

### Run Docker image without installation
You may also take advantage of Docker to run Alexon.

Save code below to a file named **alexon**:
```bash
#!/bin/bash

docker run -it --rm \
       -v $PWD:/alexon
       -v /var/run/docker.sock:/var/run/docker.sock \
       registry.gitlab.com/symecloud/alexon:latest \
       /alexon/"$@"
```

Then make it executable:

```bash
sudo mv alexon /usr/local/bin/
sudo chmod +x /usr/local/bin/alexon
```
**NOTE: Make sure you've installed Docker on you host machine!**

## Quick start

Save this yaml into a file named **my-blog.yaml**:
```yaml
apiVersion: syme.dev/alexon/app/v1-alpha
kind: Application
name: my-blog
port: "8080"
nodes:
  nodeFile:
    path: /etc/alexon/templates/colt.yaml
```

Then run command:

```bash
alexon app create my-blog.yaml
```

*NOTE: If the 8080 port was reported to be occupied, please change a port.*

Please hold on for a moment, Alexon is finishing the work. **Even Alexon command was finished, you may still need a few seconds to let Colt blog engine bootup.**

Then open localhost:8080 with you browser.

Enjoy!

## License

Alexon is free software and licensed with GPLv3+.

## Contribution

Contributions are welcome. Feel free to raise issues and MR on GitLab.

Pull requests and stars are always welcome.

For bugs and feature requests, please create an issue.

-1. Fork Alexon and clone to your machine
-2. Create a branch: git checkout -b my-new-feature
-3. Commit your changes: git commit -am 'Add some feature'
-4. Push to your fork: git push origin my-new-feature
-5. Submit your MR (merge request) and let's review it!

## Documentation

Please visit [alexon.dev](https://alexon.dev).

## Copyright

SymeCloud Limited originally developed Alexon, and it has been open-sourced with the hope of aiding individuals in achieving decentralization. You can find more information about SymeCloud Limited at the website [syme.dev](https://syme.dev).
