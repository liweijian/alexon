#! @GUILE@ \
-e main
!#

;;  -*-  indent-tabs-mode:nil; coding: utf-8 -*-
;;  Copyright (C) 2023
;;      SymeCloud Limited
;;  This is free software: you can redistribute it and/or modify
;;  it under the terms of the GNU General Public License published by
;;  the Free Software Foundation, either version 3 of the License, or
;;  (at your option) any later version.

;;  This is distributed in the hope that it will be useful,
;;  but WITHOUT ANY WARRANTY; without even the implied warranty of
;;  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;  GNU General Public License for more details.

;;  You should have received a copy of the GNU General Public License
;;  along with this program.
;;  If not, see <http://www.gnu.org/licenses/>.

(setlocale LC_ALL "")
(use-modules (ice-9 getopt-long)
             (ice-9 format)
             (alexon utils)
             (alexon commands)
             (alexon commands help))

(define *common-commands* '("help" "create" "node" "network" "cluster" "app"))

(define (is-common-command? name)
  (member name *common-commands*))

(define main
  (lambda (args)
    (cond
     ;; handle list-all-cmds here
     ((and (not (null? (cdr args))) (string=? (cadr args) "list-all-cmds"))
      (format #t "~{~a~^    ~}~%" ((@@ (alexon commands help) get-all-commands))))
     ((find-command (if (no-command? args) "help" (cadr args)))
      values =>
      (lambda (name mod)
        (cond
         ((is-common-command? name) (apply (module-ref mod 'main)
                                           (if (null? (cdr args)) '() (cddr args))))
         (else
          (parameterize ((%current-toplevel (find-ENTRY-path identity)))
            (apply (module-ref mod 'main) (if (no-command-args? args)
                                              '()
                                              (cdr args))))))))
     (else
      (format (current-error-port)
              "alexon: unknown command ~s~%" (cadr args))
      (format (current-error-port)
              "Try `alexon help' for more information.~%")
      (exit 1)))))
