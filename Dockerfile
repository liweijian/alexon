FROM        registry.gitlab.com/symecloud/guile-alpine:latest
MAINTAINER  Roy Mu <roy@syme.dev>

ARG CACHE_PKG=1
RUN apk add --no-cache make autoconf automake libtool pkgconf guile-dev bash util-linux curl

COPY . /alexon
WORKDIR /alexon

ARG CACHE_DEPS=1
RUN ./scripts/install-deps.sh --no-docker

ARG CACHE_BUILD=1
RUN ./configure && make clean && make && make install && cd .. && rm -fr alexon

ENTRYPOINT ["alexon"]
